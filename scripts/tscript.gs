var ss = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/16naNy1fYASgG4gL-rMfMVU3eSUryEJUWdLX26nMBmU0/edit#gid=0");

var sheet = ss.getSheetByName('Справочник');
function doPost(e){
  var action = e.parameter.action;
  if(action == 'addItem'){
    return addItem(e);
  }
  if(action == 'getItem'){
    return getItem(e);
  }
}

function doGet(e){
  var action = e.parameter.action;
  if(action == 'getItem'){
    return getItem(e);
  }
}

function getItem(e){

  var ss1 = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/1-BFthGRTDCp3o0UOzzL4d4Y1Dq-zW8_g057ePRVEai8/edit#gid=0");
  var sheet1 = ss1.getSheetByName('Справочник');
  var fio = sheet1.getRange(3, 9, sheet1.getColumnWidth(9), 1).getValues();
  var positionOnWork = sheet1.getRange(3, 10, sheet1.getColumnWidth(10), 1).getValues();
  var organizationOfContractor = sheet1.getRange(3, 14, sheet1.getColumnWidth(14), 1).getValues();
  var organizationOfCustomer = sheet1.getRange(3, 18, sheet1.getColumnWidth(18), 1).getValues();
  var fio = sheet1.getRange(3, 9, sheet1.getColumnWidth(9), 1).getValues();
  var fio = sheet1.getRange(3, 9, sheet1.getColumnWidth(9), 1).getValues();
  var fio = sheet1.getRange(3, 9, sheet1.getColumnWidth(9), 1).getValues();
  var jo = {};
  var dataArray = [];
  for (var i = 0, l=fio.length; i<l; i++){
    var dataRows = fio[i];
    var dataTemp = dataRows[0];
    var data = dataTemp.toString().trim();
    if(data != ""){
      var record ={};
      record['fio'] = data;
      dataArray.push(record);
    }else{
      break;
    }
  }

  jo.dataSheet = dataArray;

  var result = JSON.stringify(jo);

  return ContentService.createTextOutput(result).setMimeType(ContentService.MimeType.JSON);
}

function createFolder(e){
  var name = e.parameter.name;
  var rootFolder1 = "Test1";
  var rootFolder2 = "Администраторы";
  var folder, folders = DriveApp.getFoldersByName(rootFolder2);
  if (folders.hasNext()) {
      folder = folders.next();
    } else {
      folder = DriveApp.createFolder(rootFolder1);
    }

  var folder1, folders1 = folder.getFoldersByName(rootFolder2);
  if (folders1.hasNext()) {
      folder1 = folders1.next();
    } else {
      folder1 = folder.createFolder(rootFolder2);
    }

 folder1.createFolder(name);
  return ContentService.createTextOutput("Директория создана").setMimeType(ContentService.MimeType.TEXT);
}

  function decode(fileName, uImage){
    var dropbox = "USERS IMAGE";
    var folder, folders = DriveApp.getFoldersByName(dropbox);

    if (folders.hasNext()) {
      folder = folders.next();
    } else {
      folder = DriveApp.createFolder(dropbox);
    }

    var contentType = "image/jpg",
        bytes = Utilities.base64Decode(uImage),
        blob = Utilities.newBlob(bytes, contentType,fileName);
    var file = folder.createFile(blob);

    file.setSharing(DriveApp.Access.ANYONE_WITH_LINK,DriveApp.Permission.VIEW);
    var fileId=file.getId();

    return "https://drive.google.com/uc?export=view&id="+fileId;
  }

function addItem(e){
var number = e.parameter.number;
var date = e.parameter.date;
var personName = e.parameter.personName;
var post = e.parameter.post;
var organization = e.parameter.organization;
var customer = e.parameter.customer;
var carInfo = e.parameter.carInfo;
var driver = e.parameter.driver;
var columnNumber = e.parameter.columnNumber;
var objectNameAndAdress = e.parameter.objectNameAndAdress;
var fuelVolumeBefore = e.parameter.fuelVolumeBefore;
var fuelImageBytesBefore = e.parameter.fuelImageBytesBefore;
var fuelType = e.parameter.fuelType;
var amountOfFuelIssued = e.parameter.amountOfFuelIssued;
var odometrValueBefore = e.parameter.odometrValueBefore;
var odometrImageBytesBefore = e.parameter.odometrImageBytesBefore;
var counterValueBefore = e.parameter.counterValueBefore;
var counterImageBytesBefore = e.parameter.counterImageBytesBefore;
var beginWorkTime = e.parameter.beginWorkTime;
var endWorkTime = e.parameter.endWorkTime;
var hoursWorked = e.parameter.hoursWorked;
var fuelVolumeAfter = e.parameter.fuelVolumeAfter;
var fuelImageBytesAfter = e.parameter.fuelImageBytesAfter;
var odometrValueAfter = e.parameter.odometrValueAfter;
var odometrImageBytesAfter = e.parameter.odometrImageBytesAfter;
var counterValueAfter = e.parameter.counterValueAfter;
var counterImageBytesAfter = e.parameter.counterImageBytesAfter;
var workScopePerformed = e.parameter.workScopePerformed;
var comment = e.parameter.comment;
var unit = e.parameter.unit;
var amount = e.parameter.amount;
var stayTimeAmount = e.parameter.stayTimeAmount;
var stayCauser = e.parameter.stayCauser;
var stayReason = e.parameter.stayReason;
var generalComment = e.parameter.generalComment;

  var number1  =  " "+sheet.getLastRow();

  var fuelBeforeURL = "";
  var odometrBeforeURL = "";
  var counterBeforeURL = "";
  var fuelAfterURL = "";
  var odometrAfterURL = "";
  var counterAfterURL = "";



    var fuelBefore = number1 + "fuelBefore.jpg";
    var odometrBefore = number1 + "odometrBefore.jpg";
    var counterBefore = number1 + "counterBefore.jpg";
    var fuelAfter = number1 + "fuelAfter.jpg";
    var odometrAfter = number1 + "odometrAfter.jpg";
    var counterAfter = number1 + "counterAfter.jpg";

    if (fuelImageBytesBefore != ""){
       fuelBeforeURL = decode(fuelBefore, fuelImageBytesBefore);
    }
    if (odometrImageBytesBefore != ""){
       odometrBeforeURL = decode(odometrBefore, odometrImageBytesBefore);
    }
    if (counterImageBytesBefore != ""){
       counterBeforeURL = decode(counterBefore, counterImageBytesBefore);
    }
    if (fuelImageBytesAfter != ""){
       fuelAfterURL = decode(fuelAfter, fuelImageBytesAfter);
    }
    if (odometrImageBytesAfter != ""){
       odometrAfterURL = decode(odometrAfter, odometrImageBytesAfter);
    }
    if (counterImageBytesAfter != ""){
       counterAfterURL = decode(counterAfter, counterImageBytesAfter);
    }

sheet.appendRow([number,date,personName, post, organization, customer, carInfo, driver, columnNumber, objectNameAndAdress, objectNameAndAdress,fuelVolumeBefore, fuelBeforeURL,fuelType, amountOfFuelIssued,odometrValueBefore, odometrBeforeURL, counterValueBefore, counterBeforeURL, beginWorkTime, endWorkTime, hoursWorked, fuelVolumeAfter, fuelAfterURL, odometrValueAfter, odometrAfterURL, counterValueAfter, counterAfterURL, workScopePerformed, comment, unit, amount, stayTimeAmount, stayCauser, stayReason, generalComment]);
   return ContentService.createTextOutput("Данные успешно загружены").setMimeType(ContentService.MimeType.TEXT);
}
