package com.orego.truckapp.repo.client.annotation

/**
 * Defines that this field is agreed with the web application (App script).
 */

@Target(
    AnnotationTarget.FIELD
)
annotation class TAppScript