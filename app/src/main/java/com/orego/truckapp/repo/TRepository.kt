package com.orego.truckapp.repo

import android.util.Log
import androidx.room.Room
import com.orego.truckapp.R
import com.orego.truckapp.api.repo.gearManager.OregoGearManager
import com.orego.truckapp.core.TApplication
import com.orego.truckapp.repo.client.TAdminClient
import com.orego.truckapp.repo.client.TWorkerClient
import com.orego.truckapp.repo.client.model.admin.TAdmin
import com.orego.truckapp.repo.client.model.admin.group.TWorkerGroup
import com.orego.truckapp.repo.client.model.admin.group.manual.TManual
import com.orego.truckapp.repo.client.model.admin.group.worker.report.TReport
import com.orego.truckapp.repo.client.model.admin.group.worker.TWorker
import com.orego.truckapp.repo.data.TDatabase
import com.orego.truckapp.repo.data.entity.report.TReportEntity
import com.orego.truckapp.repo.data.entity.report.TReportTitleEntity
import com.orego.truckapp.repo.gearManager.TGearManager
import com.orego.truckapp.ui.controller.TUiWorkerReportInfoController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext

class TRepository(private val application: TApplication) {

    /**
     * Sub repositories.
     */

    val workerSubRepository by lazy {
        WorkerSubRepository(this)
    }

    val adminSubRepository by lazy {
        AdminSubRepository(this)
    }

    /**
     * Room database.
     */

    private val database by lazy {
        val context = application.applicationContext
        val clazz = TDatabase::class.java
        val name = context.getString(R.string.database_name)
        return@lazy Room
            .databaseBuilder(context, clazz, name)
            .fallbackToDestructiveMigration()
            .build()
    }

    /**
     * Worker sub parent.
     */

    class WorkerSubRepository(private val parent: TRepository) : OregoGearManager.Gear.Listener {

        /**
         * Worker client.
         */

        private val workerClient by lazy {
            TWorkerClient(parent.application.applicationContext)
        }

        /**
         * Managers.
         */

        val accountManager by lazy {
            AccountManager(this)
        }

        val reportManager by lazy {
            ReportManager(this)
        }

        val manualManager by lazy {
            ManualManager(this)
        }

        /**
         * Finishes session.
         */

        fun signOut() {
            this.accountManager.finishSession()
            this.reportManager.finishSession()
            this.manualManager.finishSession()
            this.parent.application.gearManager.apply {
                this.cancelGear(TGearManager.WorkerSignInGear::class.java)
                this.cancelGear(TGearManager.WorkerFetchManualGear::class.java)
                this.cancelGear(TGearManager.WorkerFetchReportsGear::class.java)
                this.cancelGear(TGearManager.WorkerPushReportsGear::class.java)
            }
        }

        override fun onGearFinished(clazz: Class<out OregoGearManager.Gear>, result: Any?) {
            println("GEAR CALLBACK: $clazz")
            if (clazz == TGearManager.WorkerFetchReportsGear::class.java) {
                val gearManager = this.parent.application.gearManager
                gearManager.launchGear(TGearManager.WorkerPushReportsGear::class.java)
            }
        }

        /**
         * Worker manager.
         */

        class AccountManager(private val parent: WorkerSubRepository) {

            companion object {

                private const val WORKER_ACCOUNT_FILE = "worker_account"

                private const val LAST_WORKER_KEY = "last"
            }

            /**
             * Shared preferences.
             */

            private val sharedPreferences by lazy {
                this.parent.parent.application.preferenceManager
            }

            /**
             * Properties.
             */

            var currentWorker: TWorker? = null

            fun tryAutoSignIn(): Boolean {
                val lastWorker = this.sharedPreferences.getData<TWorker>(
                    WORKER_ACCOUNT_FILE,
                    LAST_WORKER_KEY
                ) ?: return false
                Log.i("OGO_ILYA", "tryAutoSignIn")
                this.currentWorker = lastWorker
                return true
            }

            suspend fun signIn(
                login: String,
                password: String,
                adminEmail: String,
                groupName: String
            ): TWorkerClient.AuthController.ResponseType =
                withContext(Dispatchers.IO) {
                    val client = this@AccountManager.parent.workerClient
                    val response = client.authController.requestAuth2(login, password, adminEmail, groupName)

                    //Check response:
                    if (response.first != TWorkerClient.AuthController.ResponseType.SUCCESS) {
                        return@withContext response.first
                    }

                    //Set worker:
                    val worker = response.second
                    this@AccountManager.currentWorker = worker
                    Log.i("OGO_ILYA", "currentWorker = worker from cloud")
                    this@AccountManager.sharedPreferences.setData(WORKER_ACCOUNT_FILE, LAST_WORKER_KEY, worker)
                    println("SIGN IN WORKER: " + this@AccountManager.currentWorker)

                    //Return success type.
                    return@withContext TWorkerClient.AuthController.ResponseType.SUCCESS
                }

            fun finishSession() {
                this.currentWorker = null
                Log.i("OGO_ILYA", "currentWorker = null")
                this.sharedPreferences.setData<TWorker>(WORKER_ACCOUNT_FILE, LAST_WORKER_KEY, null)
            }
        }

        /**
         * Report.
         */

        class ReportManager(private val parent: WorkerSubRepository) : CoroutineScope {

            override val coroutineContext = Dispatchers.Main

            companion object {

                private const val PROGRESS_KEY = "progress"

                private const val REPORT_KEY = "report"
            }

            /**
             * Shared preferences.
             */

            private val sharedPreferences by lazy {
                this.parent.parent.application.preferenceManager
            }

            /**
             * Property.
             */

            var uncompletedReport = TReport()

            var progress = Progress.FIRST_FORM_NOT_STARTED

            var completedReportTitles = mutableListOf<TReportTitleEntity>()

            /**
             * Prepares report materials for current account.
             */

            suspend fun beginSession() = withContext(Dispatchers.IO) {
                val task1 = async {
                    this@ReportManager.digUncompletedReport()
                }
                val task2 = async {
                    this@ReportManager.digCompletedReportTitles()
                }
                //Wait tasks:
                task2.await()
                task1.await()
            }

            suspend fun sendUndeliveredReports(): Boolean = withContext(Dispatchers.IO) {
                val accountManager = this@ReportManager.parent.accountManager
                val currentAccount = accountManager.currentWorker
                val reportGoogleSheetUrl = currentAccount?.reportUrl ?: return@withContext false
                val database = this@ReportManager.parent.parent.database
                val reportController = this@ReportManager.parent.workerClient.reportController
                this@ReportManager.completedReportTitles.forEach { titleEntity ->
                    if (!titleEntity.hasDelivered && titleEntity.isComplete) {
                        val reportEntity = this@ReportManager.digCompletedReport(titleEntity.id)
                        val response = reportController.postNewReport(reportEntity.report, reportGoogleSheetUrl)
                        if (response == TWorkerClient.ReportController.ResponseType.SUCCESS) {
                            reportEntity.hasDelivered = true
                            titleEntity.hasDelivered = true
                            database.reportDao.update(reportEntity)
                            database.reportTitleDao.update(titleEntity)
                        }
                    }
                }
                return@withContext !completedReportTitles.any { it.isComplete && !it.hasDelivered }
            }

            fun digReportProgress() {
                val accountManager = this@ReportManager.parent.accountManager
                val currentAccount = accountManager.currentWorker
                val reportGoogleSheetUrl = currentAccount?.reportUrl ?: return
                val key = reportGoogleSheetUrl.replace('/', '|')
                Log.i("OGO_ILYA", "key = " + key)
                val typeString = this@ReportManager.sharedPreferences.getData<String>(key, PROGRESS_KEY)
                if (typeString != null) {
                    Log.i("OGO_ILYA", "key = " + typeString)
                    this@ReportManager.progress = Progress.valueOf(typeString)
                }
            }

            /**
             * Uncompleted report.
             */

            suspend fun digUncompletedReport(): Boolean = withContext(Dispatchers.IO) {
                val accountManager = this@ReportManager.parent.accountManager
                val currentAccount = accountManager.currentWorker
                val reportGoogleSheetUrl = currentAccount?.reportUrl ?: return@withContext false
                val key = reportGoogleSheetUrl.replace('/', '|')
                val typeString = this@ReportManager.sharedPreferences.getData<String>(key, PROGRESS_KEY)
                val report = this@ReportManager.sharedPreferences.getData<TReport>(key, REPORT_KEY)
                if (typeString != null && report != null) {
                    this@ReportManager.progress = Progress.valueOf(typeString)
                    this@ReportManager.uncompletedReport = report
                    return@withContext true
                }
                return@withContext false
            }

            suspend fun digCompletedReportTitles() = withContext(Dispatchers.IO) {
                val accountManager = this@ReportManager.parent.accountManager
                val currentAccount = accountManager.currentWorker
                val reportGoogleSheetUrl = currentAccount?.reportUrl ?: return@withContext
                val database = this@ReportManager.parent.parent.database
                val reports = database.reportTitleDao.getReports(reportGoogleSheetUrl)
                this@ReportManager.completedReportTitles = reports.toMutableList()
                println("COMPLETED LOCAL_REPORT_GEAR: " + this@ReportManager.completedReportTitles)
            }

            suspend fun digCompletedReportTitlesById(id: Long) = withContext(Dispatchers.IO) {
                val database = this@ReportManager.parent.parent.database
                return@withContext database.reportTitleDao.getReport(id)
            }

            suspend fun digCompletedReport(reportId: Long) = withContext(Dispatchers.IO) {
                val database = this@ReportManager.parent.parent.database
                return@withContext database.reportDao.getReport(reportId)
            }

            fun groundUncompletedReport() {
                val accountManager = this.parent.accountManager
                val currentAccount = accountManager.currentWorker
                val reportGoogleSheetUrl = currentAccount?.reportUrl ?: return
                val key = reportGoogleSheetUrl.replace('/', '|')
                this.sharedPreferences.setData(key, PROGRESS_KEY, this.progress)
                this.sharedPreferences.setData(key, REPORT_KEY, this.uncompletedReport)
            }

            suspend fun groundUncompletedTitleReport() =
                withContext(Dispatchers.IO) {
                    val completedReport = this@ReportManager.uncompletedReport
                    this@ReportManager.parent.workerClient
                    val accountManager = this@ReportManager.parent.accountManager
                    val account = accountManager.currentWorker ?: return@withContext TWorkerClient.ReportController
                        .ResponseType.NOT_SIGNED_ACCOUNT
                    val reportUrl = account.reportUrl
                    val database = this@ReportManager.parent.parent.database
                    val reportEntity = TReportEntity(
                        report = completedReport,
                        reportUrl = reportUrl
                    )
                    val id = database.reportDao.insert(reportEntity)
                    reportEntity.id = id
                    val reportTitleEntity = TReportTitleEntity(
                        id,
                        completedReport.number,
                        completedReport.personName,
                        completedReport.driver,
                        reportUrl = reportUrl
                    )
                    database.reportTitleDao.insert(reportTitleEntity)
                    this@ReportManager.completedReportTitles.add(reportTitleEntity)
                }


            /**
             * Sends a report to the server.
             */

            suspend fun sendCompletedReport(reportController: TUiWorkerReportInfoController): TWorkerClient.ReportController.ResponseType =
                withContext(Dispatchers.IO) {
                    val reportEntity = reportController.entityReport!!
                    reportEntity.isSending = true
                    val client = this@ReportManager.parent.workerClient
                    val accountManager = this@ReportManager.parent.accountManager
                    val account = accountManager.currentWorker ?: return@withContext TWorkerClient.ReportController
                        .ResponseType.NOT_SIGNED_ACCOUNT
                    val reportUrl = account.reportUrl
                    val database = this@ReportManager.parent.parent.database

                    println("ENTITY ID" + reportEntity.id)
                    database.reportDao.update(reportEntity)

                    val reportTitleEntity = digCompletedReportTitlesById(reportEntity.id!!)
                    reportTitleEntity.isComplete = true
                    database.reportTitleDao.update(reportTitleEntity)
                    //Check response:
                    val response = client.reportController.postNewReport(reportEntity.report, reportUrl)
                    println("ADD LOCAL_REPORT_GEAR>>>: $response")
                    if (response != TWorkerClient.ReportController.ResponseType.SUCCESS) {
                        reportTitleEntity.hasDelivered = false
                        reportEntity.isSending = false
                        reportEntity.hasDelivered = false
                        database.reportDao.update(reportEntity)
                        database.reportTitleDao.update(reportTitleEntity)
                        val oldReport = this@ReportManager.completedReportTitles.find { it.id == reportEntity.id }
                        this@ReportManager.completedReportTitles.remove(oldReport)
                        this@ReportManager.completedReportTitles.add(reportTitleEntity)
                        return@withContext TWorkerClient.ReportController.ResponseType.BAD_RESPONSE
                    }
                    reportEntity.hasDelivered = true
                    reportTitleEntity.hasDelivered = true
                    reportEntity.isSending = false
                    database.reportDao.update(reportEntity)
                    database.reportTitleDao.update(reportTitleEntity)

                    val oldReport = this@ReportManager.completedReportTitles.find { it.id == reportEntity.id }
                    this@ReportManager.completedReportTitles.remove(oldReport)
                    this@ReportManager.completedReportTitles.add(reportTitleEntity)
                    this@ReportManager.groundUncompletedReport()

                    return@withContext TWorkerClient.ReportController.ResponseType.SUCCESS
                }

            fun finishSession() {
                progress = Progress.FIRST_FORM_NOT_STARTED
                this.uncompletedReport = TReport()
                this.completedReportTitles = mutableListOf()

            }

            suspend fun groundReport2(reportEntity: TReportEntity) = withContext(Dispatchers.IO) {
                val database = this@ReportManager.parent.parent.database
                database.reportDao.update(reportEntity)
            }


            /**
             * Defines report progress.
             */

            enum class Progress {
                FIRST_FORM_NOT_STARTED,
                FIRST_FORM_STARTED,

            }
        }

        /**
         * Manual manager.
         */

        class ManualManager(private val parent: WorkerSubRepository) : CoroutineScope {

            companion object {

                private const val MANUAL_KEY = "manual"
            }

            override val coroutineContext = Dispatchers.Main

            /**
             * Shared preferences.
             */

            private val sharedPreferences by lazy {
                this.parent.parent.application.preferenceManager
            }

            var currentManual: TManual? = null

            suspend fun beginSession() = withContext(Dispatchers.IO) {
                val accountManager = this@ManualManager.parent.accountManager
                val currentAccount = accountManager.currentWorker ?: return@withContext
                val manualUrl = currentAccount.manualUrl
                val formattedUrl = manualUrl.replace('/', '|')
                if (this@ManualManager.digManual(formattedUrl)) {
                    if (this@ManualManager.downloadManual(manualUrl)) {
                        this@ManualManager.groundManual(formattedUrl)
                    }
                    return@withContext
                }
                while (!this@ManualManager.downloadManual(manualUrl)) {
                }
                this@ManualManager.groundManual(formattedUrl)
            }

            /**
             * Downloads manual.
             */

            suspend fun downloadManual(manualUrl: String): Boolean {
                val client = this.parent.workerClient
                //Get response:
                val response = client.manualController.requestManual(manualUrl)
                if (response.first == TWorkerClient.ManualController.ResponseType.SUCCESS) {
                    this.currentManual = response.second
                    Log.i("SYNC_MANUAL_GEAR", response.second!!.toString())
                    return true
                }
                return false
            }

            fun groundManual(formattedManualUrl: String) {
                this.sharedPreferences.setData(MANUAL_KEY, formattedManualUrl, this.currentManual)
            }

            fun digManual(formattedManualUrl: String): Boolean {
                val manual = this.sharedPreferences.getData<TManual>(MANUAL_KEY, formattedManualUrl)
                if (manual != null) {
                    this.currentManual = manual
                    return true
                }
                return false
            }

            fun finishSession() {
                this.currentManual = null
            }
        }
    }

    /**
     * Admin.
     */

    class AdminSubRepository(private val parent: TRepository) {


        val adminClient by lazy {
            TAdminClient(parent.application.baseContext)
        }

        val accountManager by lazy {
            AccountManager(this)
        }

        val groupManager by lazy {
            GroupManager(this)
        }

        val workerManager by lazy {
            WorkerManager(this)
        }

        /**
         * Sign out.
         */

        fun signOut() {
            this.accountManager.finishSession()

            //CHANGE:
            this.groupManager.finishSession()
            this.workerManager.finishSession()
            this.parent.application.gearManager.apply {
                this.cancelGear(TGearManager.WorkerSignInGear::class.java)
                this.cancelGear(TGearManager.WorkerFetchManualGear::class.java)
                this.cancelGear(TGearManager.WorkerFetchReportsGear::class.java)
                this.cancelGear(TGearManager.WorkerPushReportsGear::class.java)
            }
        }

        /**
         * Account manager.
         */

        class AccountManager(private val parent: AdminSubRepository) {

            companion object {

                private const val ADMIN_ACCOUNT_FILE = "admin_account"

                private const val LAST_ADMIN_KEY = "last"
            }

            /**
             * Shared preferences.
             */

            private val sharedPreferences by lazy {
                this.parent.parent.application.preferenceManager
            }

            /**
             * Properties.
             */

            var currentAdmin: TAdmin? = null

            fun tryAutoSignIn(): Boolean {
                val lastAdmin = this.sharedPreferences.getData<TAdmin>(
                    ADMIN_ACCOUNT_FILE,
                    LAST_ADMIN_KEY
                ) ?: return false
                this.currentAdmin = lastAdmin
                return true
            }

            suspend fun signIn(phoneNumber: String, email: String): TAdminClient.AuthController.ResponseType =
                withContext(Dispatchers.IO) {
                    val accountManager = this@AccountManager
                    val client = accountManager.parent.adminClient
                    val response = client.authController.requestAuth(phoneNumber, email)
                    //Check response:
                    val responseType = response.first
                    if (responseType != TAdminClient.AuthController.ResponseType.SUCCESS) {
                        return@withContext responseType
                    }
                    //Set admin:
                    val admin = response.second
                        ?: return@withContext TAdminClient.AuthController.ResponseType.BAD_RESPONSE
                    accountManager.currentAdmin = admin
                    accountManager.sharedPreferences.setData(ADMIN_ACCOUNT_FILE, LAST_ADMIN_KEY, admin)
                    println("SIGN IN ADMIN: " + accountManager.currentAdmin)
                    //Return success type:
                    return@withContext responseType
                }

            fun finishSession() {
                this.currentAdmin = null
                this.sharedPreferences.setData<TWorker>(
                    ADMIN_ACCOUNT_FILE, LAST_ADMIN_KEY, null
                )
            }
        }

        /**
         * Group manager.
         */

        class GroupManager(private val parent: AdminSubRepository) {

            var groups = mutableListOf<TWorkerGroup>()

            suspend fun downloadGroups(): TAdminClient.GroupController.ResponseType? =
                withContext(Dispatchers.IO) {
                    //Get url:
                    val adminSubRepository = this@GroupManager.parent
                    val accountManager = adminSubRepository.accountManager
                    val currentAdmin = accountManager.currentAdmin ?: return@withContext null
                    val groupsUrl = currentAdmin.groupsUrl
                    //Request workers:
                    val adminClient = this@GroupManager.parent.adminClient
                    val controller = adminClient.groupController
                    val response = controller.requestGroups(groupsUrl)
                    val responseType = response.first
                    if (responseType == TAdminClient.GroupController.ResponseType.SUCCESS) {
                        val downloadedGroups = response.second!!
                        this@GroupManager.groups = downloadedGroups
                    }
                    return@withContext responseType
                }

            suspend fun createGroup(uncompletedGroup: TWorkerGroup):
                    Pair<TAdminClient.GroupController.ResponseType, TWorkerGroup?>? =
                withContext(Dispatchers.IO) {
                    //Get url:
                    val accountManager = this@GroupManager.parent.accountManager
                    val currentAdmin = accountManager.currentAdmin ?: return@withContext null
                    val groupsUrl = currentAdmin.groupsUrl
                    //Post group:
                    val controller = this@GroupManager.parent.adminClient.groupController
                    val response = controller.postGroup(groupsUrl, uncompletedGroup)
                    val responseType = response.first
                    Log.i("LOG_COMBACA", "responseType$responseType")
                    if (responseType == TAdminClient.GroupController.ResponseType.SUCCESS) {
                        val newGroup = response.second!!
                        this@GroupManager.groups.add(newGroup)
                    }
                    return@withContext response
                }

            fun finishSession() {
                this.groups = mutableListOf()
            }
        }

        /**
         * Worker manager.
         */

        class WorkerManager(private val parent: AdminSubRepository) {

            /**
             * Property.
             */

            var workers = mutableListOf<TWorker>()

            suspend fun downloadWorkers(workersUrl: String): TAdminClient.WorkerController.ResponseType =
                withContext(Dispatchers.IO) {
                    //Request workers:
                    val controller = this@WorkerManager.parent.adminClient.workerController
                    val response = controller.requestWorkers(workersUrl)
                    val responseType = response.first
                    if (responseType == TAdminClient.WorkerController.ResponseType.SUCCESS) {
                        val downloadedWorkers = response.second!!
                        this@WorkerManager.workers = downloadedWorkers
                    }
                    return@withContext responseType
                }


            suspend fun createWorker(groupUrl: String, worker: TWorker):
                    Pair<TAdminClient.WorkerController.ResponseType, TWorker?> =
                withContext(Dispatchers.IO) {
                    val controller = this@WorkerManager.parent.adminClient.workerController
                    val response = controller.postWorker(groupUrl, worker)
                    val responseType = response.first
                    if (responseType == TAdminClient.WorkerController.ResponseType.SUCCESS) {
                        val createdWorker = response.second!!
                        this@WorkerManager.workers.add(createdWorker)
                    }
                    return@withContext response
                }

            fun finishSession() {
                this.workers = mutableListOf()
            }
        }
    }
}