package com.orego.truckapp.repo.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.orego.truckapp.repo.data.dao.report.TReportDao
import com.orego.truckapp.repo.data.dao.report.TReportTitleDao
import com.orego.truckapp.repo.data.entity.report.TReportEntity
import com.orego.truckapp.repo.data.entity.report.TReportTitleEntity

@Database(
    entities = [
        TReportEntity::class,
        TReportTitleEntity::class
    ],
    version = 3
)
abstract class TDatabase : RoomDatabase() {

    /**
     * Dao.
     */

    abstract val reportDao: TReportDao

    abstract val reportTitleDao : TReportTitleDao
}