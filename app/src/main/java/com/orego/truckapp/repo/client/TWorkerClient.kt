package com.orego.truckapp.repo.client

import android.content.Context
import android.util.Log
import com.android.volley.NoConnectionError
import com.android.volley.VolleyError
import com.orego.truckapp.api.util.client.OregoVolleyClientComponent
import com.orego.truckapp.api.util.common.json.fromJson
import com.orego.truckapp.repo.client.annotation.TAppScript
import com.orego.truckapp.repo.client.model.admin.group.manual.TManual
import com.orego.truckapp.repo.client.model.admin.group.worker.report.TReport
import com.orego.truckapp.repo.client.model.admin.group.worker.report.toMap
import com.orego.truckapp.repo.client.model.admin.group.worker.TWorker
import com.google.gson.JsonSyntaxException

class TWorkerClient(private val context: Context) : OregoVolleyClientComponent {

    companion object {

        @TAppScript
        private const val APP_SCRIPT_URL =
            "https://script.google.com/macros/s/AKfycbxslPzfvec4S8jGykX9QYz47bNnRPUMpKnyf2EVSXbgmT7HIXI/exec"
    }

    /**
     * App script url.
     */

    override val url = APP_SCRIPT_URL

    /**
     * Controller.
     */

    val authController by lazy {
        AuthController(this)
    }

    val reportController by lazy {
        ReportController(this)
    }

    val manualController by lazy {
        ManualController(this)
    }

    /**
     * Post.
     */

    private suspend fun makeVolleyPost(map: Map<String, String>): String? {
        return this.makeVolleyPost(map, this.context)
    }

    /**
     * Auth controller.
     */

    class AuthController(private val client: TWorkerClient) {

        companion object {

            @Deprecated ("use AUTH2")
            @TAppScript
            private const val AUTH = "auth"

            @TAppScript
            private const val AUTH2 = "auth2"

            @TAppScript
            private const val INVALID_AUTH_PARAMS = "INVALID_AUTH_PARAMS"

        }

        /**
         * Sign in.
         */
        @Deprecated("use AUTH2")
        suspend fun requestAuth(login: String, password: String): Pair<ResponseType, TWorker?> {
            //Compose request:
            val map = mapOf(
                "action" to AUTH,
                "login" to login,
                "password" to password
            )
            try {
                val response = this.client.makeVolleyPost(map)
                //Check response:
                if (response == INVALID_AUTH_PARAMS) {
                    return ResponseType.INVALID_AUTH_PARAMS to null
                }
                val worker = response.fromJson<TWorker>() ?: return ResponseType.BAD_RESPONSE to null

                //Return success type.
                return ResponseType.SUCCESS to worker
            } catch (e: VolleyError) {
                return ResponseType.NO_CONNECTION to null
            } catch (e: JsonSyntaxException) {
                return ResponseType.BAD_RESPONSE to null
            }  catch (e : NoConnectionError) {
                return ResponseType.NO_CONNECTION to null
            }
        }

        suspend fun requestAuth2(login: String, password: String, adminEmail:String, groupName:String): Pair<ResponseType, TWorker?> {
            //Compose request:
            val map = mapOf(
                "action" to AUTH2,
                "login" to login,
                "password" to password,
                "adminEmail" to adminEmail,
                "group" to groupName
            )
            try {
                val response = this.client.makeVolleyPost(map)
                //Check response:
                if (response == INVALID_AUTH_PARAMS) {
                    return ResponseType.INVALID_AUTH_PARAMS to null
                }
                val worker = response.fromJson<TWorker>() ?: return ResponseType.BAD_RESPONSE to null

                //Return success type.
                return ResponseType.SUCCESS to worker
            } catch (e: VolleyError) {
                return ResponseType.NO_CONNECTION to null
            } catch (e: JsonSyntaxException) {
                return ResponseType.BAD_RESPONSE to null
            }  catch (e : NoConnectionError) {
                return ResponseType.NO_CONNECTION to null
            }
        }

        /**
         * Response type.
         */

        enum class ResponseType {
            INVALID_AUTH_PARAMS,
            BAD_RESPONSE,
            NO_CONNECTION,
            SUCCESS
        }
    }

    /**
     * Report controller.
     */

    class ReportController(private val client: TWorkerClient) {

        companion object {

            @TAppScript
            private const val SUCCESS = "SUCCESS"

            @TAppScript
            private const val REQUEST_ACTION = "addReport"
        }

        suspend fun postNewReport(report: TReport, reportUrl: String): ResponseType {
            //Compose request:
            val reportMap = report.toMap()
            val requestMap = mutableMapOf(
                "action" to REQUEST_ACTION,
                "reportUrl" to reportUrl
            )
            for (entry in reportMap) {
                requestMap[entry.key] = entry.value
                val logList = listOf<String>("odometrImageBytesBefore", "counterImageBytesBefore", "fuelImageBytesBefore", "odometrImageBytesAfter", "counterImageBytesAfter", "fuelImageBytesAfter")
                if (!logList.contains(entry.key))
                Log.i("DATA_REPORT", "KEY: ${entry.key} VALUE: ${entry.value}")
            }
            try {
                val response = this.client.makeVolleyPost(requestMap)
                if (response != SUCCESS) {
                    return ResponseType.BAD_RESPONSE
                }
                return ResponseType.SUCCESS
            } catch (e: VolleyError) {
                return ResponseType.NO_CONNECTION
            }  catch (e : NoConnectionError) {
                return ResponseType.NO_CONNECTION
            }
        }

        /**
         * Defines response type.
         */

        enum class ResponseType {
            SUCCESS,
            NO_CONNECTION,
            BAD_RESPONSE,
            NOT_SIGNED_ACCOUNT
        }
    }

    /**
     * Manual controller.
     */

    class ManualController(private val client: TWorkerClient) {

        companion object {

            @TAppScript
            private const val GET_MANUAL_ACTION = "getManual"
        }

        suspend fun requestManual(manualUrl : String): Pair<ResponseType, TManual?> {
            val map = mapOf(
                "action" to GET_MANUAL_ACTION,
                "manualUrl" to manualUrl
            )
            return try {
                val response = this.client.makeVolleyPost(map)
                val manual = response.fromJson<TManual>()
                ResponseType.SUCCESS to manual
            } catch (e: VolleyError) {
                println("START LOAD SYNC_MANUAL_GEAR NO CONNECTION")
                ResponseType.NO_CONNECTION to null
            } catch (e: JsonSyntaxException) {
                println("START LOAD SYNC_MANUAL_GEAR BAD RESPONSE")
                ResponseType.BAD_RESPONSE to null
            } catch (e: NoConnectionError) {
                ResponseType.NO_CONNECTION to null
            }
        }

        /**
         * Response type.
         */

        enum class ResponseType {
            SUCCESS,
            NO_CONNECTION,
            BAD_RESPONSE,
            NOT_SIGNED_ACCOUNT
        }
    }
}