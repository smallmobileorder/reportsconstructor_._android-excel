package com.orego.truckapp.repo.client

import android.content.Context
import android.util.Log
import com.android.volley.NoConnectionError
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import com.orego.truckapp.api.util.client.OregoVolleyClientComponent
import com.orego.truckapp.api.util.common.json.fromJson
import com.orego.truckapp.api.util.common.json.fromJsonToMutableList
import com.orego.truckapp.repo.client.annotation.TAppScript
import com.orego.truckapp.repo.client.model.admin.TAdmin
import com.orego.truckapp.repo.client.model.admin.group.TWorkerGroup
import com.orego.truckapp.repo.client.model.admin.group.worker.TWorker

class TAdminClient(private val context: Context) : OregoVolleyClientComponent {

    companion object {

        @TAppScript
        private const val APP_SCRIPT_URL =
            "https://script.google.com/macros/s/AKfycbyB5fqtGr_OGz-ZjGsZ-WI1jUyK1SrW4sP_u8MhLXe8YuQ1Nn4/exec"
    }

    /**
     * App script url.
     */

    override val url = APP_SCRIPT_URL

    /**
     * Controller.
     */

    val authController by lazy {
        AuthController(this)
    }

    val groupController by lazy {
        GroupController(this)
    }

    val workerController by lazy {
        WorkerController(this)
    }

    /**
     * Post.
     */

    private suspend fun makeVolleyPost(map: Map<String, String>): String? {
        return this.makeVolleyPost(map, this.context)
    }

    /**
     * Auth controller.
     */

    class AuthController(private val client: TAdminClient) {

        companion object {

            @TAppScript
            private const val AUTH = "auth"

            @TAppScript
            private const val INVALID_AUTH_PARAMS = "INVALID_AUTH_PARAMS"
        }

        /**
         * Sign in.
         */

        suspend fun requestAuth(phone: String, email: String): Pair<ResponseType, TAdmin?> {
            //Compose request:
            val map = mapOf(
                "action" to AUTH,
                "phone" to phone,
                "email" to email
            )
            return try {
                val response = this.client.makeVolleyPost(map)
                //Check response:
                if (response == INVALID_AUTH_PARAMS) {
                    ResponseType.INVALID_AUTH_PARAMS to null
                } else {
                    val worker = response.fromJson<TAdmin>()
                    if (worker == null) {
                        ResponseType.BAD_RESPONSE to null
                    } else {
                        //Return success type.
                        ResponseType.SUCCESS to worker
                    }
                }
            } catch (e: VolleyError) {
                ResponseType.NO_CONNECTION to null
            } catch (e: JsonSyntaxException) {
                ResponseType.BAD_RESPONSE to null
            } catch (e : NoConnectionError) {
                ResponseType.NO_CONNECTION to null
            }
        }

        /**
         * Response type.
         */

        enum class ResponseType {
            INVALID_AUTH_PARAMS,
            BAD_RESPONSE,
            NO_CONNECTION,
            SUCCESS
        }
    }

    /**
     * Group controller.
     */

    class GroupController(private val client: TAdminClient) {

        companion object {

            @TAppScript
            private const val GET_GROUPS = "getGroups"

            @TAppScript
            private const val ADD_GROUP = "addGroup"
        }

        suspend fun requestGroups(groupsUrl: String): Pair<ResponseType, MutableList<TWorkerGroup>?> {
            //Compose request:
            val map = mapOf(
                "action" to GET_GROUPS,
                "groupsUrl" to groupsUrl
            )
            try {
                val response = this.client.makeVolleyPost(map)
                //Check response:
                Log.i("GROUPS RESPONSE", response)
                val typeToken = object : TypeToken<MutableList<TWorkerGroup>>() {}.type
                val workers = Gson().fromJson<MutableList<TWorkerGroup>>(response, typeToken)
                    ?: return ResponseType.BAD_RESPONSE to null
                Log.i("GROUPS RESPONSE", "SUCCESS")
                Log.i("GROUPS RESPONSE 2", workers.toString())
                for (worker in workers) {
                    Log.i("IO", worker.toString())
                }
                //Return success type.
                return ResponseType.SUCCESS to workers
            } catch (e: VolleyError) {
                return ResponseType.NO_CONNECTION to null
            } catch (e: JsonSyntaxException) {
                return ResponseType.BAD_RESPONSE to null
            } catch (e : NoConnectionError) {
                return ResponseType.NO_CONNECTION to null
            }
        }

        suspend fun postGroup(groupsUrl: String, uncompletedGroup: TWorkerGroup): Pair<ResponseType, TWorkerGroup?> {
            //Compose request:
            val map = mutableMapOf(
                "action" to ADD_GROUP,
                "groupsUrl" to groupsUrl,
                "groupName" to uncompletedGroup.groupName
            )
            return try {
                val response = this.client.makeVolleyPost(map)
                if (response == "GROUP_IS_EXIST") return ResponseType.IS_EXIST to null
                val group = response.fromJson<TWorkerGroup>()
                    ?: return ResponseType.BAD_RESPONSE to null
                ResponseType.SUCCESS to group
            } catch (e: VolleyError) {
                e.printStackTrace()
                ResponseType.NO_CONNECTION to null
            } catch (e: JsonSyntaxException) {
                ResponseType.BAD_RESPONSE to null
            } catch (e : NoConnectionError) {
                ResponseType.NO_CONNECTION to null
            }
        }

        /**
         * Response type.
         */

        enum class ResponseType {
            BAD_RESPONSE,
            NO_CONNECTION,
            SUCCESS,
            IS_EXIST
        }
    }

    /**
     * Worker controller.
     */

    class WorkerController(private val client: TAdminClient) {

        companion object {

            @TAppScript
            private const val GET_WORKERS = "getWorkers"

            @TAppScript
            private const val ADD_WORKER = "addWorker"
        }

        suspend fun requestWorkers(workersUrl: String): Pair<ResponseType, MutableList<TWorker>?> {
            //Compose request:
            val map = mapOf(
                "action" to GET_WORKERS,
                "workersUrl" to workersUrl
            )
            try {
                val response = this.client.makeVolleyPost(map)
                //Check response:
                val typeToken = object : TypeToken<MutableList<TWorker>>() {}.type
                val workers = Gson().fromJson<MutableList<TWorker>>(response, typeToken)
                    ?: return ResponseType.BAD_RESPONSE to null
                for (worker in workers) {
                    Log.i("WORKERS RESPONSE: ", worker.toString())
                }
                //Return success type.
                return ResponseType.SUCCESS to workers
            } catch (e: VolleyError) {
                return ResponseType.NO_CONNECTION to null
            } catch (e: JsonSyntaxException) {
                return ResponseType.BAD_RESPONSE to null
            }  catch (e : NoConnectionError) {
                return ResponseType.NO_CONNECTION to null
            }
        }

        suspend fun postWorker(groupUrl: String, worker: TWorker): Pair<ResponseType, TWorker?> {
            //Compose request:
            val map = mutableMapOf(
                "action" to ADD_WORKER,
                "workersUrl" to groupUrl
            )
            map.putAll(worker.toMap())
            return try {
                val response = this.client.makeVolleyPost(map)
                if (response == "WORKER_IS_EXIST") {
                    return ResponseType.IS_EXIST to null
                }
                val createdWorker = response.fromJson<TWorker>()
                    ?: return ResponseType.BAD_RESPONSE to null
                ResponseType.SUCCESS to createdWorker
            } catch (e: VolleyError) {
                ResponseType.NO_CONNECTION to null
            } catch (e: JsonSyntaxException) {
                ResponseType.BAD_RESPONSE to null
            }  catch (e : NoConnectionError) {
                return ResponseType.NO_CONNECTION to null
            }
        }

        private fun TWorker.toMap() = mapOf(
            "login" to this.login,
            "key" to this.password,
            "name" to this.name,
            "post" to this.post,
            "phone" to this.phone,
            "manualUrl" to this.manualUrl
        )

        /**
         * Response type.
         */

        enum class ResponseType {
            BAD_RESPONSE,
            NO_CONNECTION,
            SUCCESS,
            IS_EXIST
        }
    }
}