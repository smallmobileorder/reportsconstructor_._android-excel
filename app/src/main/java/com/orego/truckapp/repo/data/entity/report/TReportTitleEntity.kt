package com.orego.truckapp.repo.data.entity.report

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "report_title")
data class TReportTitleEntity(

    @PrimaryKey
    var id: Long,

    var reportNumber: String,

    var personName: String,

    var driver: String,

    var isComplete: Boolean = false,

    /**
     * Has delivered.
     */

    var hasDelivered: Boolean = false,

    /**
     * Report url.
     */

    var reportUrl: String
)