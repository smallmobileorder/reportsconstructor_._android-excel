package com.orego.truckapp.repo.data.dao.report

import androidx.room.*
import com.orego.truckapp.repo.data.entity.report.TReportTitleEntity

@Dao
interface TReportTitleDao {

    @Insert(
        onConflict = OnConflictStrategy.REPLACE
    )
    fun insert(report: TReportTitleEntity): Long

    @Update(
        onConflict = OnConflictStrategy.REPLACE
    )
    fun update(report: TReportTitleEntity)

    @Query(
        "SELECT * FROM report_title WHERE reportUrl = :reportGoogleSheetUrl"
    )
    fun getReports(reportGoogleSheetUrl : String): List<TReportTitleEntity>

    @Query("SELECT * FROM report_title WHERE id = :titleId")
    fun getReport(titleId:Long): TReportTitleEntity

}