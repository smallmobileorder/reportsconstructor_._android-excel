package com.orego.truckapp.repo.client.model.admin.group.manual

data class TManual(
    var names: MutableSet<String> = mutableSetOf(),
    var posts: MutableSet<String> = mutableSetOf(),
    var executors: MutableSet<String> = mutableSetOf(),
    var customers: MutableSet<String> = mutableSetOf(),
    var cars: MutableSet<String> = mutableSetOf(),
    var drivers: MutableSet<String> = mutableSetOf(),
    var columns: MutableSet<String> = mutableSetOf(),
    var sites: MutableSet<String> = mutableSetOf(),
    var fuelTypes: MutableSet<String> = mutableSetOf(),
    var objects: MutableSet<String> = mutableSetOf(),
    var metrics: MutableSet<String> = mutableSetOf(),
    var causers : MutableSet<String> = mutableSetOf()
)