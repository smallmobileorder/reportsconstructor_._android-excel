package com.orego.truckapp.repo.client.model.admin.group

class TWorkerGroup(
    var groupName: String = "",
    var workersUrl: String = "",
    var manualUrl: String = ""
)