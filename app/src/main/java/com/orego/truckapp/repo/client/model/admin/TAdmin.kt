package com.orego.truckapp.repo.client.model.admin

data class TAdmin(
    var phone: String = "",
    var email: String = "",
    var name: String = "",
    var groupsUrl: String =""
)