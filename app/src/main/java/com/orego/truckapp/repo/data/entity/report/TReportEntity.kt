package com.orego.truckapp.repo.data.entity.report

import androidx.room.*
import com.orego.truckapp.repo.client.model.admin.group.worker.report.TReport

@Entity(
    tableName = "report"
)
data class TReportEntity(

    /**
     * Local id.
     */

    @PrimaryKey(
        autoGenerate = true
    )
    @ColumnInfo(
        name = "report_id"
    )
    var id: Long? = null,

    /**
     * Is delivered to the server.
     */

    @ColumnInfo(
        name = "report_has_delivered"
    )
    var hasDelivered : Boolean = false,

    /**
     * Report url.
     */

    var reportUrl : String,

    /**
     * Report data.
     */

    @Embedded
    var report: TReport,

    var isSending: Boolean = false
)