package com.orego.truckapp.repo.data.dao.report

import androidx.room.*
import com.orego.truckapp.repo.data.entity.report.TReportEntity

@Dao
interface TReportDao {

    @Insert(
        onConflict = OnConflictStrategy.REPLACE
    )
    fun insert(report: TReportEntity): Long

    @Update(
        onConflict = OnConflictStrategy.REPLACE
    )
    fun update(report: TReportEntity)

    @Query(
        "SELECT * FROM report WHERE report_id = :id"
    )
    fun getReport(id: Long) : TReportEntity
}