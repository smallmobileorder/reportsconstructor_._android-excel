package com.orego.truckapp.repo.client.model.admin.group.worker.report

data class TReport(

    /**
     * Form 1.
     */

    var isOdometr:String = true.toString(),
    var number: String = "",
    var date: String = "",
    var personName: String = "",
    var post: String = "",
    var organization: String = "",
    var customer: String = "",
    var carInfo: String = "",
    var driver: String = "",
    var columnNumber: String = "",
    var siteNumber : String = "",
    var objectNameAndAddress: String = "",
    var fuelVolumeBefore: String = "",
    var fuelImageBytesBefore: String = "",
    var fuelType: String = "",
    var amountOfFuelIssued: String = "",
    var odometrValueBefore: String = "",
    var odometrImageBytesBefore: String = "",
    var counterValueBefore: String = "",
    var counterImageBytesBefore: String = "",
    var beginWorkTime: String = "",

    /**
     * Form 2.
     */

    var endWorkTime: String = "",
    var hoursWorked: String = "",
    var fuelVolumeAfter: String = "",
    var fuelImageBytesAfter: String = "",
    var odometrValueAfter: String = "",
    var odometrImageBytesAfter: String = "",
    var counterValueAfter: String = "",
    var counterImageBytesAfter: String = "",
    var workScopePerformed: String = "",
    var comment: String = "",
    var unit: String = "",
    var amount: String = "",
    var stayTimeAmount: String = "",
    var stayCauser: String = "",
    var stayReason: String = "",
    var generalComment: String = ""
)

fun TReport.toMap() = mapOf(
    //Form 1:
    "number" to this.number,
    "date" to this.date,
    "personName" to this.personName,
    "post" to this.post,
    "organization" to this.organization,
    "customer" to this.customer,
    "carInfo" to this.carInfo,
    "driver" to this.driver,
    "columnNumber" to this.columnNumber,
    "siteNumber" to this.siteNumber,
    "objectNameAndAddress" to this.objectNameAndAddress,
    "fuelVolumeBefore" to this.fuelVolumeBefore,
    "fuelImageBytesBefore" to this.fuelImageBytesBefore,
    "fuelType" to this.fuelType,
    "amountOfFuelIssued" to this.amountOfFuelIssued,
    "odometrValueBefore" to this.odometrValueBefore,
    "odometrImageBytesBefore" to this.odometrImageBytesBefore,
    "counterValueBefore" to this.counterValueBefore,
    "counterImageBytesBefore" to this.counterImageBytesBefore,
    "beginWorkTime" to this.beginWorkTime,
    //Form 2:
    "endWorkTime" to this.endWorkTime,
    "hoursWorked" to this.hoursWorked,
    "fuelVolumeAfter" to this.fuelVolumeAfter,
    "fuelImageBytesAfter" to this.fuelImageBytesAfter,
    "odometrValueAfter" to this.odometrValueAfter,
    "odometrImageBytesAfter" to this.odometrImageBytesAfter,
    "counterValueAfter" to this.counterValueAfter,
    "counterImageBytesAfter" to this.counterImageBytesAfter,
    "workScopePerformed" to this.workScopePerformed,
    "comment" to this.comment,
    "unit" to this.unit,
    "amount" to this.amount,
    "stayTimeAmount" to this.stayTimeAmount,
    "stayCauser" to this.stayCauser,
    "stayReason" to this.stayReason,
    "generalComment" to this.generalComment
)