package com.orego.truckapp.repo.gearManager

import com.orego.truckapp.api.repo.gearManager.OregoGearManager
import com.orego.truckapp.core.TApplication
import com.orego.truckapp.repo.client.model.admin.group.TWorkerGroup
import com.orego.truckapp.ui.fragment.implementation.admin.TAdminAddWorkerFragment
import com.orego.truckapp.ui.fragment.implementation.admin.TAdminSignInFragment
import com.orego.truckapp.ui.fragment.implementation.worker.TWorkerSignInFragment

class TGearManager(application: TApplication) : OregoGearManager(application) {

    private val repository by lazy {
        application.repository
    }

    /**
     * Add gears
     */

    init {

        //Worker:
        this.addGear(WorkerSignInGear())
        this.addGear(WorkerFetchReportsGear())
        this.addGear(WorkerFetchManualGear())
        this.addGear(WorkerPushReportsGear())
//        this.addGear() TODO:

        //Admin:
        this.addGear(AdminSignInGear())
        this.addGear(AdminFetchGroupsGear())
        this.addGear(AdminFetchWorkersGear())
        this.addGear(AdminPostGroupGear())
        this.addGear(AdminPostWorkerGear())
    }

    /**
     * Gear.
     */

    /**
     * Worker.
     */

    inner class WorkerSignInGear : Gear() {

        override suspend fun apply(params: Any?): Any? {
            if (params is TWorkerSignInFragment.AuthParams) {
                val accountManager = this@TGearManager.repository.workerSubRepository.accountManager
                return accountManager.signIn(params.login, params.password, params.adminEmail, params.groupName)
            }
            return null
        }

        init {
            val workerRepository = this@TGearManager.repository.workerSubRepository
            this.addListener(workerRepository)
        }
    }

    inner class WorkerFetchReportsGear : Gear() {

        override suspend fun apply(params: Any?): Any? {
            val reportManager = this@TGearManager.repository.workerSubRepository.reportManager
            reportManager.beginSession()
            return true
        }
    }

    inner class WorkerFetchManualGear : Gear() {

        override suspend fun apply(params: Any?): Any? {
            val manualManager = this@TGearManager.repository.workerSubRepository.manualManager
            manualManager.beginSession()
            return true
        }
    }

    inner class WorkerPushReportsGear : Gear() {

        override suspend fun apply(params: Any?): Any? {
            return repository.workerSubRepository.reportManager.sendUndeliveredReports()
        }
    }

    /**
     * Admin.
     */

    inner class AdminSignInGear : Gear() {

        /**
         * Authorizes admin.
         */

        override suspend fun apply(params: Any?): Any? {
            if (params !is TAdminSignInFragment.AuthParams) {
                return null
            }
            val repository = this@TGearManager.repository
            val subRepository = repository.adminSubRepository
            val accountManager = subRepository.accountManager
            return accountManager.signIn(params.phoneNumber, params.email)
        }
    }

    inner class AdminFetchGroupsGear : Gear() {

        /**
         * Downloads admin groups.
         */

        override suspend fun apply(params: Any?): Any? {
            val repository = this@TGearManager.repository
            val subRepository = repository.adminSubRepository
            val groupManager = subRepository.groupManager
            return groupManager.downloadGroups()
        }
    }

    inner class AdminFetchWorkersGear : Gear() {

        override suspend fun apply(params: Any?): Any? {
            if (params is String) {
                val workerManager = this@TGearManager.repository.adminSubRepository.workerManager
                return workerManager.downloadWorkers(params)
            }
            return null
        }
    }

    inner class AdminPostGroupGear : Gear() {

        override suspend fun apply(params: Any?): Any? {
            if (params is TWorkerGroup) {
                val groupManager = this@TGearManager.repository.adminSubRepository.groupManager
                return groupManager.createGroup(params)
            }
            return null
        }
    }

    inner class AdminPostWorkerGear : Gear() {

        override suspend fun apply(params: Any?): Any? {
            if (params is TAdminAddWorkerFragment.WorkerParams) {
                val workerManager = this@TGearManager.repository.adminSubRepository.workerManager
                return workerManager.createWorker(params.workersUrl, params.worker)
            }
            return null
        }
    }
}