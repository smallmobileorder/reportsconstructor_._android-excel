package com.orego.truckapp.repo.client.model.admin.group.worker

data class TWorker(
    var login: String = "",
    var password: String = "",
    var name: String = "",
    var post: String = "",
    var phone : String = "",
    var reportUrl: String = "",
    var manualUrl: String = ""
)