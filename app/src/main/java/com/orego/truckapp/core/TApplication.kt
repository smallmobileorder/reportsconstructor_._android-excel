package com.orego.truckapp.core

import com.orego.truckapp.api.core.OregoApplication
import com.orego.truckapp.repo.TRepository
import com.orego.truckapp.repo.gearManager.TGearManager
import com.orego.truckapp.ui.controller.TUiAdminGroupInfoController
import com.orego.truckapp.ui.controller.TUiAdminWorkerInfoController
import com.orego.truckapp.ui.controller.TUiWorkerReportInfoController
import com.orego.truckapp.ui.fragment.implementation.admin.*
import com.orego.truckapp.ui.fragment.implementation.common.TCommonStartFragment
import com.orego.truckapp.ui.fragment.implementation.worker.*

class TApplication : OregoApplication() {

    /**
     * Repository.
     */

    val repository by lazy {
        TRepository(this)
    }

    /**
     * Task manager.
     */

    override val gearManager by lazy {
        TGearManager(this)
    }

    /**
     * Fragments.
     */

    override val fragments = listOf(

        //Common:
        TCommonStartFragment(),

        //Admin:
        TAdminSignInFragment(),

        TAdminGroupsFragment(),
        TAdminWorkersFragment(),
        TAdminAddWorkerFragment(),

        //Worker:
        TWorkerSignInFragment(),
        TWorkerReportsFragment(),
        TReportForm1Fragment(),
        TReportForm2Fragment(),
        TWorkerReportInfoFragment()
    )

    /**
     * Controller.
     */

    val workerReportInfoController by lazy {
        TUiWorkerReportInfoController()
    }

    val adminGroupInfoController by lazy {
        TUiAdminGroupInfoController()
    }

    val adminWorkerInfoController by lazy {
        TUiAdminWorkerInfoController()
    }
}