package com.orego.truckapp.core

import android.content.Context
import android.net.ConnectivityManager
import com.orego.truckapp.R
import com.orego.truckapp.api.core.OregoActivity
import com.orego.truckapp.ui.fragment.implementation.common.TCommonStartFragment
import com.orego.truckapp.ui.fragment.implementation.worker.TWorkerSignInFragment

class TActivity : OregoActivity(R.layout.activity_main) {

    override val fragmentContainerId = R.id.a_fragment

    /**
     * Application.
     */

    override val oregoApplication
        get() = this.application as TApplication

    /**
     * Start fragment.
     */

    override val initialFragmentClazz = TCommonStartFragment::class.java


    fun isOnline(): Boolean {
        val cs = Context.CONNECTIVITY_SERVICE
        val cm = getSystemService(cs) as ConnectivityManager
        return cm.activeNetworkInfo != null
    }
}