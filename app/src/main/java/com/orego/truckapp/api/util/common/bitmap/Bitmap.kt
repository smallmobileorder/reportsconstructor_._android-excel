package com.orego.truckapp.api.util.common.bitmap

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import java.io.ByteArrayOutputStream

fun Bitmap?.toByteArray(format: Bitmap.CompressFormat, quality: Int): ByteArray {
    val byteArrayOutputStream = ByteArrayOutputStream()
    this?.compress(format, quality, byteArrayOutputStream)
    return byteArrayOutputStream.toByteArray()
}

/**
 * Bitmap.
 */

fun Bitmap.toStringBytes(dimensions: Int = 250): String {
    val rbitmap = this.getResizedBitmap(dimensions)
    return rbitmap.getStringImage()
}

private fun Bitmap.getResizedBitmap(maxSize: Int): Bitmap {
    var width = this.width
    var height = this.height
    val bitmapRatio = width.toFloat() / height.toFloat()
    if (bitmapRatio > 1) {
        width = maxSize
        height = (width / bitmapRatio).toInt()
    } else {
        height = maxSize
        width = (height * bitmapRatio).toInt()
    }
    return Bitmap.createScaledBitmap(this, width, height, true)

}

private fun Bitmap.getStringImage(
    format: Bitmap.CompressFormat = Bitmap.CompressFormat.JPEG,
    quality: Int = 100
): String {
    val byteArrayOutputStream = ByteArrayOutputStream()
    this.compress(format, quality, byteArrayOutputStream)
    val imageBytes = byteArrayOutputStream.toByteArray()
    return Base64.encodeToString(imageBytes, Base64.DEFAULT)
}

fun String.toBitmap(): Bitmap {
    val decodedString = Base64.decode(this, Base64.DEFAULT)
    return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
}