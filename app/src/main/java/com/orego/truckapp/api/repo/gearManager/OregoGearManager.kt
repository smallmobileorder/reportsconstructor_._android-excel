package com.orego.truckapp.api.repo.gearManager

import com.orego.truckapp.api.core.OregoApplication
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

open class OregoGearManager(private val application: OregoApplication) : CoroutineScope {

    override val coroutineContext = Dispatchers.Main

    private val gearMap: MutableMap<Class<out Gear>, Gear> = mutableMapOf()

    fun launchGear(clazz: Class<out Gear>, params : Any? = null) {
        val application = this@OregoGearManager.application
        val gear = this.gearMap[clazz] ?: return
        val job = gear.job
        if (job == null || !job.isActive) {
            gear.job = this.launch {
                val result = gear.invoke(params)
                application.fragmentController.onGearFinished(clazz, result)
                gear.job = null
            }
        }
    }

    fun registerListener(clazz: Class<out Gear>, listener: Gear.Listener) {
        val gear = this.gearMap[clazz] ?: return
        gear.addListener(listener)
    }

    fun unregisterListener(clazz: Class<out Gear>, listener: Gear.Listener) {
        val gear = this.gearMap[clazz] ?: return
        gear.removeListener(listener)
    }

    fun addGear(gear: Gear) {
        this.gearMap[gear::class.java] = gear
    }

    fun cancelGear(clazz : Class<out Gear>) {
        val gear = this.gearMap[clazz] ?: return
        println("CANCEL $clazz")
        gear.job?.cancel()
        gear.job = null
    }

    /**
     * Gear.
     */

    abstract class Gear {

        var job: Job? = null

        protected open val listeners = mutableSetOf<Listener>()

        protected abstract suspend fun apply(params : Any?) : Any?

        suspend fun invoke(params: Any?) : Any? {
            val result = this.apply(params)
            val listeners = this.listeners.toList()
            val size = listeners.size
            for (i in 0 until size) {
                val listener = listeners[i]
                listener.onGearFinished(this::class.java, result)
            }
            return result
        }

        fun addListener(listener: Listener) {
            this.listeners += listener
        }

        fun removeListener(listener: Listener) {
            this.listeners -= listener
        }

        /**
         * Listener.
         */

        interface Listener {

            fun onGearFinished(clazz : Class<out Gear>, result: Any?)
        }
    }
}