package com.orego.truckapp.api.repo.sharedPrefereces

import android.content.Context.MODE_PRIVATE
import com.orego.truckapp.api.core.OregoApplication
import com.orego.truckapp.api.util.common.json.fromJson
import com.google.gson.Gson

open class OregoPreferenceManager(val application: OregoApplication) {

    inline fun <reified T> getData(fileName: String?, key: String?): T? =
        this.application
            .getSharedPreferences(fileName, MODE_PRIVATE)
            .getString(key, null).fromJson<T>()

    inline fun <reified T> setData(fileName: String?, key: String?, value: T?) {
        val gson = Gson()
        val data = gson.toJson(value, T::class.java)
        val editor = this.application
            .getSharedPreferences(fileName, MODE_PRIVATE)
            .edit()
        editor.putString(key, data)
        editor.apply()
    }
}