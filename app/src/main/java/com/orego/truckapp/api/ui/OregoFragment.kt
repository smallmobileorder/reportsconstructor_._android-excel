package com.orego.truckapp.api.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.orego.truckapp.api.core.OregoApplication
import com.orego.truckapp.api.repo.gearManager.OregoGearManager
import kotlinx.coroutines.delay

/**
 * @author Игорь Гулькин 14.07.2018
 */

abstract class OregoFragment(private val layoutId: Int) : Fragment() {

    /**
     * Single application.
     */

    lateinit var application: OregoApplication

    var isCreated = false

    private lateinit var root: View

    /**
     * Presenter.
     */

    protected open val presenter: Presenter = Presenter()

    final override fun onCreateView(i: LayoutInflater, c: ViewGroup?, b: Bundle?): View {
        if (!this.isCreated) {
            this.root = i.inflate(this.layoutId, c, false)!!
        }
        return this.root
    }

    final override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (!this.isCreated) {
            this.isCreated = true
            this.onFirstViewCreated()
        }
    }

    protected open fun onFirstViewCreated() {

    }

    override fun onStart() {
        super.onStart()
        this.presenter.onStart()
    }

    override fun onStop() {
        super.onStop()
        this.presenter.onStop()
    }

    /**
     * Handles back click.
     */

    open fun onBackPressed() = this.presenter.onBackPressed()

    /**
     * Callback.
     */

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        this.presenter.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        this.presenter.onActivityResult(requestCode, resultCode, data)
    }

    open fun onGearFinished(clazz: Class<out OregoGearManager.Gear>, result: Any?) {
//        println("GEAR FRAGMENT CALLBACK $clazz ${this::class.java}")
        this.presenter.onGearFinished(clazz, result)
    }

    /**
     * Presenter.
     */

    open inner class Presenter {

        open val application
            get() = this@OregoFragment.application

        open val preferenceManager
            get() = this.application.preferenceManager

        open val gearManager: OregoGearManager
            get() = this.application.gearManager

        open fun onStart() {

        }

        open fun onStop() {

        }

        open fun onGearFinished(gearClazz: Class<out OregoGearManager.Gear>, result: Any?) {

        }

        protected fun openFragment(clazz: Class<out OregoFragment>?, bundle: Bundle? = null) {
            if (clazz != null) {
                val fragmentController = this.application.fragmentController
                val supportFragmentManager = activity?.supportFragmentManager ?: return
                fragmentController.openFragment(clazz, supportFragmentManager, bundle)
            }
        }

        fun makeToast(message: String?) {
            Toast.makeText(this@OregoFragment.context, message, Toast.LENGTH_SHORT).show()
        }

        protected suspend fun makeToastWithDelay(message: String?, delayMillis: Long = 1000) {
            this.makeToast(message)
            delay(delayMillis)
        }

        protected fun recycleFragment(fragment: OregoFragment) {
            this.application.fragmentController.recycleFragment(fragment)
        }

        /**
         * On back pressed callback.
         */

        open fun onBackPressed() = false

        /**
         * Callback.
         */

        open fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        }

        open fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        }

        fun isDisplayed(): Boolean {
            val fragmentController = this.application.fragmentController
            return this@OregoFragment::class.java == fragmentController.displayedFragmentClazz
        }
    }
}