package com.orego.truckapp.api.util.ui.component

import android.content.Context
import android.content.Intent
import android.net.Uri
import com.orego.truckapp.api.util.common.account.OregoAccountUtils

interface OregoCallComponent {

    fun callPhoneNumber(phoneNumber : String, context: Context) {
        if (!OregoAccountUtils.isCorrectPhoneNumber(phoneNumber)) {
            return
        }
        val formattedPhoneNumber = OregoAccountUtils.formatPhoneNumber(phoneNumber)
        val callIntent = Intent(Intent.ACTION_CALL)
        callIntent.data = Uri.parse("tel:$formattedPhoneNumber")
        context.startActivity(callIntent)
    }
}