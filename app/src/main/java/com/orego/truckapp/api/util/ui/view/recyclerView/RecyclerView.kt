package com.orego.truckapp.api.util.ui.view.recyclerView

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.configure(context: Context?, adapter : RecyclerView.Adapter<*>) {
    this.adapter = adapter
    this.layoutManager = LinearLayoutManager(context)
    this.setHasFixedSize(true)
}