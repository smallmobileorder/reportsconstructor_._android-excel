package com.orego.truckapp.api.util.common.account

object OregoAccountUtils {

    fun isCorrectPhoneNumber(string: String) =
        string.matches("^((\\+7|7|8)+([0-9]){10})|^$".toRegex())
                && string.isNotEmpty()

    fun isCorrectEmail(string: String) = string
        .matches("^[_A-Za-z0-9+]+(.[_A-Za-z0-9]+)*@[A-Za-z0-9-]+(.[A-Za-z0-9]+)*(.[A-Za-z]{2,})$".toRegex())
            && string.isNotEmpty()

    fun isCorrectLogin(string: String) = string.matches("^[a-zа-яA-ZА-Я0-9]+|^$".toRegex())
            && string.isNotEmpty()

    fun formatPhoneNumber(phone: String) = phone
        .replace("""\D+""".toRegex(), "")
        .replace("^8|\\+7".toRegex(), "7")

    fun formatLogin(login:String) = login.replace("""\D+""".toRegex(),"")
}