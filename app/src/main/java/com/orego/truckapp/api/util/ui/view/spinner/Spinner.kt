package com.orego.truckapp.api.util.ui.view.spinner

import android.content.Context
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner

fun Spinner.configure(list: List<String>, context: Context, itemLayoutId: Int, callback: (String) -> Unit) {
    val stringList = mutableListOf<String>().also { it.addAll(list) }
    this.adapter = ArrayAdapter<String>(context, itemLayoutId, stringList)
    this.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            if (parent != null) {
                val selectedString = parent.getItemAtPosition(position).toString()
                callback(selectedString)
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {
        }
    }
}

fun Spinner.getSelectedValue(): String {
    return if (this.selectedItem == null) ""
    else
        this.selectedItem.toString()
}

inline fun <reified T> Spinner.setSelectionBy(value: T) {
    val adapter = this.adapter
    val itemCount = adapter.count
    for (i in 0 until itemCount) {
        val item = adapter.getItem(i) as T
        if (value == item) {
            this.setSelection(i)
        }
    }
}
