package com.orego.truckapp.api.util.ui.view.textView

import android.widget.TextView

fun TextView.setLimitText(text: String, limit: Int = 10) {
    var string = text
    if (text.length > limit) {
        string = text.substring(0,limit) + "..."
    }
    this.text = string
}