package com.orego.truckapp.api.util.ui.view

import android.view.View

fun View.setOnClickListener(event: () -> Unit) {
    this.setOnClickListener { event() }
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}
