package com.orego.truckapp.api.core

import android.app.Application
import com.orego.truckapp.api.repo.sharedPrefereces.OregoPreferenceManager
import com.orego.truckapp.api.repo.gearManager.OregoGearManager
import com.orego.truckapp.api.ui.OregoFragment

abstract class OregoApplication : Application() {

    /**
     * Orego task manager.
     */

    open val gearManager by lazy {
        OregoGearManager(this)
    }

    /**
     * Shared preferences.
     */

    open val preferenceManager by lazy {
        OregoPreferenceManager(this)
    }

    /**
     * Ui.
     */

    open val fragmentController by lazy {
        OregoFragmentController(this, this.fragments)
    }

    protected abstract val fragments : List<OregoFragment>
}