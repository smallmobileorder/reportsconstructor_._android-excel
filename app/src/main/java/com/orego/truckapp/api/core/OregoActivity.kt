package com.orego.truckapp.api.core

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.orego.truckapp.api.ui.OregoFragment

abstract class OregoActivity(private val layoutId: Int) : AppCompatActivity() {

    abstract val fragmentContainerId : Int

    open val oregoApplication
        get() = this.application as OregoApplication

    private val fragmentController
        get () = this.oregoApplication.fragmentController

    protected abstract val initialFragmentClazz: Class<out OregoFragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.setContentView(this.layoutId)
    }

    override fun onStart() {
        super.onStart()
        this.setStartFragment()
    }

    private fun setStartFragment() {
        val fragmentManager = this.supportFragmentManager
        val controller = this.fragmentController
        val isFirstLaunch = controller.configure(this.fragmentContainerId, fragmentManager)
        val startFragment =
            if (isFirstLaunch) {
                this.initialFragmentClazz
            } else {
                controller.displayedFragmentClazz!!
            }
        controller.openFragment(startFragment, fragmentManager)
    }

    override fun onBackPressed() {
        if (!this.fragmentController.onBackPressed()) {
            super.onBackPressed()
        }
    }

    /**
     * Callback.
     */

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        this.fragmentController.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        this.fragmentController.onActivityResult(requestCode, resultCode, data)
    }
}