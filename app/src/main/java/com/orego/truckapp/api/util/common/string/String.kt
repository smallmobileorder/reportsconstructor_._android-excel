package com.orego.truckapp.api.util.common.string

fun String.isNumber() = this.matches("\\d+".toRegex())