package com.orego.truckapp.api.util.common.json

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

inline fun <reified T> String?.fromJson(): T? = Gson().fromJson(this, T::class.java)


//DOESN'T WORK!
inline fun <reified T> String.fromJsonToMutableList(): MutableList<T> {
    val typeToken = object : TypeToken<MutableList<T>>() {}.type
    return Gson().fromJson<MutableList<T>>(this, typeToken)
}