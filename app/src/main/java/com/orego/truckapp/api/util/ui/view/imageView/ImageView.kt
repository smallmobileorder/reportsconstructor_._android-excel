package com.orego.truckapp.api.util.ui.view.imageView

import android.widget.ImageView
import androidx.core.graphics.drawable.toBitmap
import com.orego.truckapp.api.util.common.bitmap.toStringBytes

fun ImageView.getBytes() : String {
    val bitmap = this.drawable.toBitmap()
    return bitmap.toStringBytes()
}