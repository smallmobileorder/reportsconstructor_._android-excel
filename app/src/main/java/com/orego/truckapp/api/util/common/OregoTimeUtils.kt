package com.orego.truckapp.api.util.common

import android.annotation.SuppressLint
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object OregoTimeUtils {

    @SuppressLint("SimpleDateFormat")
    fun getCurrentTime(): String {
        val simpleDateFormat = SimpleDateFormat("HH:mm")
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        return simpleDateFormat.format(calendar.time)
    }

    @SuppressLint("SimpleDateFormat")
    fun isValidDate(dateToValidate: String?, dateFormat: String): Boolean {
        if (dateToValidate == null) {
            return false
        }
        val simpleDateFormat = SimpleDateFormat(dateFormat)
        simpleDateFormat.isLenient = false
        return try {
            simpleDateFormat.parse(dateToValidate)
            true
        } catch (e: ParseException) {
            false
        }
    }

    fun calcTimeDiff(start: String, end: String): String {
        if (!OregoTimeUtils.isValidDate(start, "HH:mm")
            || !OregoTimeUtils.isValidDate(end, "HH:mm")
        ) {
            return ""
        }
        val startParts = start.split(':')
        val endParts = end.split(':')
        try {
            val allStartMinutes = startParts[0].toInt() * 60 + startParts[1].toInt()
            val allEndMinutes = endParts[0].toInt() * 60 + endParts[1].toInt()
            val diff = allEndMinutes - allStartMinutes
            if (diff < 0) {
                return ""
            }
            var hoursWorked = (diff / 60).toString()
            if (hoursWorked.length == 1) {
                hoursWorked = "0$hoursWorked"
            }
            var minutesWorked = (diff % 60).toString()
            if (minutesWorked.length == 1) {
                minutesWorked = "0$minutesWorked"
            }
            return "$hoursWorked:$minutesWorked"
        } catch (e: Exception) {
            return ""
        }
    }
}