package com.orego.truckapp.api.util.ui.view.editText

import android.widget.EditText

fun EditText.getInputText() = this.text.toString().trim()

fun EditText.clearInputText() = this.text.clear()