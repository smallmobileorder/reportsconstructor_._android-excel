package com.orego.truckapp.api.util.client

import android.content.Context
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.RetryPolicy
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

interface OregoVolleyClientComponent {

    companion object {

        private const val VOLLEY_REQUEST_TAG = "volley_request"
    }

    val url: String

    suspend fun makeVolleyPost(
        map: Map<String, String?>,
        context: Context,
        policy: RetryPolicy = DefaultRetryPolicy(
            50000,
            5,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
    ): String? = suspendCancellableCoroutine { continuation ->
        val queue = Volley.newRequestQueue(context)
        continuation.invokeOnCancellation { queue.cancelAll(VOLLEY_REQUEST_TAG) }
        //Create listeners:
        val responseListener = Response.Listener<String> { response -> continuation.resume(response.trim()) }
        val errorListener = Response.ErrorListener { error -> continuation.resumeWithException(error) }
        //Create request:
        val stringRequest = OregoStringRequest(map, this.url, responseListener, errorListener)
        stringRequest.tag = VOLLEY_REQUEST_TAG
        stringRequest.retryPolicy = policy
        //Push request:
        queue.add(stringRequest)
    }

    private class OregoStringRequest(
        private val map: Map<String, String?>,
        url: String,
        responseListener: Response.Listener<String>,
        errorListener: Response.ErrorListener
    ) : StringRequest(Method.POST, url, responseListener, errorListener) {

        override fun getParams() = this.map
    }
}