package com.orego.truckapp.api.util.ui.component

import android.content.Context
import android.widget.Toast
import es.dmoral.toasty.Toasty

interface OregoToastyComponent {

    val context : Context

    fun makeErrorToasty(message : String) {
        Toasty.error(this.context, message, Toast.LENGTH_SHORT, true).show()
    }

    fun makeSuccessToasty(message: String) {
        Toasty.success(this.context, message, Toast.LENGTH_SHORT, true).show()
    }
}