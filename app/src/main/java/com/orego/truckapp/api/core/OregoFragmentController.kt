package com.orego.truckapp.api.core

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import com.orego.truckapp.api.repo.gearManager.OregoGearManager
import com.orego.truckapp.api.ui.OregoFragment

/**
 * Controls application fragments.
 */

open class OregoFragmentController(application: OregoApplication, fragments: List<OregoFragment>) {

    /**
     * Fragments.
     */

    private val fragmentMap: MutableMap<Class<out OregoFragment>, OregoFragment> = mutableMapOf()

    /**
     * Current fragment.
     */

    private var displayedFragment: OregoFragment? = null

    val displayedFragmentClazz: Class<out OregoFragment>?
        get() {
            val fragment = this.displayedFragment
            fragment ?: return null
            return fragment::class.java
        }

    private var fragmentContainerId: Int? = null

    /**
     * Boot flag.
     */

    private var isStarted = false

    init {
        fragments.forEach { fragment ->
            fragment.application = application
            this.fragmentMap[fragment::class.java] = fragment
        }
    }

    fun configure(fragmentContainerId: Int, manager: FragmentManager): Boolean {
        if (this.isStarted) {
            return false
        }
        this.fragmentContainerId = fragmentContainerId
        this.isStarted = true
        return true
    }

    fun openFragment(clazz: Class<out OregoFragment>, manager: FragmentManager, bundle: Bundle? = null) {
        val containerId = this.fragmentContainerId ?: return
        val nextFragment = this.fragmentMap[clazz] ?: return
        if (nextFragment.arguments != null) {
            nextFragment.arguments = bundle
        }
        if (manager.isDestroyed) return
        manager
            .beginTransaction()
            .replace(containerId, nextFragment)
            .commit()
        this.displayedFragment = nextFragment
    }

    fun onBackPressed(): Boolean {
        val fragment = this.displayedFragment ?: return false
        return fragment.onBackPressed()
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        this.displayedFragment?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        this.displayedFragment?.onActivityResult(requestCode, resultCode, data)
    }

    fun recycleFragment(nextFragment: OregoFragment) {
        val previousFragment = this.fragmentMap[nextFragment::class.java] ?: return
        nextFragment.application = previousFragment.application
        this.fragmentMap[nextFragment::class.java] = nextFragment
    }

    fun onGearFinished(clazz: Class<out OregoGearManager.Gear>, result: Any?) {
        val fragments = this.fragmentMap.values
        for (fragment in fragments) {
            fragment.onGearFinished(clazz, result)
        }
    }
}