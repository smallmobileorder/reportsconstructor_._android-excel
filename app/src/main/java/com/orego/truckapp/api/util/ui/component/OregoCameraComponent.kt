package com.orego.truckapp.api.util.ui.component

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.provider.MediaStore
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.orego.truckapp.api.core.OregoActivity
import org.intellij.lang.annotations.MagicConstant

interface OregoCameraComponent {

    companion object {

        @MagicConstant
        private const val CAMERA_REQUEST_CODE = 777

        @MagicConstant
        private const val CAMERA_PERMISSION_CODE = 10012

        private const val DATA_KEY = "data"

        private const val ERROR_MESSAGE = "Камера недоступна"
    }

    val activity : OregoActivity

    fun takePhoto() {
        val activity = this.activity
        val cameraPermissionResult = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
        if (cameraPermissionResult != PackageManager.PERMISSION_GRANTED) {
            val permissions = arrayOf(Manifest.permission.CAMERA)
            ActivityCompat.requestPermissions(activity, permissions, CAMERA_PERMISSION_CODE)
        } else {
            val photoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            activity.startActivityForResult(photoIntent, CAMERA_REQUEST_CODE)
        }
    }

    fun onCameraRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == CAMERA_PERMISSION_CODE && grantResults.isNotEmpty()) {
            val cameraPermissionResult = grantResults[0]
            if (cameraPermissionResult == PackageManager.PERMISSION_GRANTED) {
                this.takePhoto()
            } else {
                this.displayError()
            }
        }
    }

    private fun displayError() {
        val context = this.activity
        val toast = Toast.makeText(context, ERROR_MESSAGE, Toast.LENGTH_LONG)
        toast.show()
    }

    /**
     * Returns activity results.
     */

    fun onCameraActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data == null || data.extras == null || data.extras.get(DATA_KEY) == null){
                this.onTakePhoto(null)
            }
            else {
                val extras = data.extras
                val bitmap = extras?.get(DATA_KEY)
                if (bitmap is Bitmap) {
                    this.onTakePhoto(bitmap)
                }
            }
        }
    }

    fun onTakePhoto(bitmap: Bitmap?)
}