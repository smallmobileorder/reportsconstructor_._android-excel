package com.orego.truckapp.ui.util.ui.image

import android.content.res.Resources
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.util.Base64
import androidx.core.graphics.drawable.toDrawable

fun String?.toDrawable(resources: Resources): Drawable {
    val decodedString = Base64.decode(this, Base64.DEFAULT)
    val bitmap =  BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
    return bitmap.toDrawable(resources)
}