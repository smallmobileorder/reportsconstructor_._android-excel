package com.orego.truckapp.ui.fragment.implementation.worker

import android.content.Intent
import android.graphics.Bitmap
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Spinner
import com.orego.truckapp.R
import com.orego.truckapp.api.util.common.OregoTimeUtils
import com.orego.truckapp.api.util.common.bitmap.toBitmap
import com.orego.truckapp.api.util.common.bitmap.toStringBytes
import com.orego.truckapp.api.util.common.string.isNumber
import com.orego.truckapp.api.util.ui.component.OregoCameraComponent
import com.orego.truckapp.api.util.ui.component.OregoToastyComponent
import com.orego.truckapp.api.util.ui.view.editText.getInputText
import com.orego.truckapp.api.util.ui.view.gone
import com.orego.truckapp.api.util.ui.view.hide
import com.orego.truckapp.api.util.ui.view.show
import com.orego.truckapp.api.util.ui.view.spinner.configure
import com.orego.truckapp.api.util.ui.view.spinner.getSelectedValue
import com.orego.truckapp.api.util.ui.view.spinner.setSelectionBy
import com.orego.truckapp.core.TActivity
import com.orego.truckapp.core.TApplication
import com.orego.truckapp.repo.TRepository
import com.orego.truckapp.repo.client.TWorkerClient
import com.orego.truckapp.repo.client.model.admin.group.worker.report.TReport
import com.orego.truckapp.repo.gearManager.TGearManager
import com.orego.truckapp.ui.fragment.TAbstractFragment
import com.orego.truckapp.ui.util.photoType.PhotoType
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_report_form_2.*
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*


class TReportForm2Fragment : TAbstractFragment(R.layout.fragment_report_form_2) {

    /**
     * Presenter.
     */

    override val presenter = Presenter()

    /**
     * Flag for cleaning user ui components.
     */

    private val tapplication by lazy {
        application as TApplication
    }


    /**
     * Default image.
     */

    private val defaultCameraDrawable by lazy {
        this@TReportForm2Fragment.resources.getDrawable(R.drawable.ic_camera)
    }

    override fun onFirstViewCreated() {
        this.configureListeners()
    }

    private fun configureListeners() {
        this.frf2_take_fuel_photo_button.setOnClickListener {
            this.presenter.takePhoto(PhotoType.FUEL)
        }
        this.frw2_take_odometr_photo_button.setOnClickListener {
            this.presenter.takePhoto(PhotoType.ODOMETR)
        }
        this.frf2_finish_button.setOnClickListener {
            this.presenter.sendReport()
        }
        this.frf2_back_button.setOnClickListener {
            this.onBackPressed()
        }
    }

    /**
     * Start.
     */

    override fun onStart() {
        super.onStart()
        this.start()
    }

    fun odometerClick() {
        val uncompletedReport = tapplication.workerReportInfoController.entityReport!!.report
        uncompletedReport.isOdometr = true.toString()
        frf2_odometr_header_text_view.text = getString(R.string.odometer_value_at_the_end_of_work)
    }

    fun counterClick() {
        val uncompletedReport = tapplication.workerReportInfoController.entityReport!!.report
        uncompletedReport.isOdometr = false.toString()
        frf2_odometr_header_text_view.text = getString(R.string.counter_value_at_the_end_of_work)
    }

    private fun start() {
        if (this.areGearsReady) {
            val isSending = this.tapplication.workerReportInfoController.entityReport!!.isSending
            if (!isSending) {
                this.frf2_scroll_view.show()
                this.frf2_back_button.show()
                this.frf2_progress_bar.gone()
                this.frf2_sending_header_text_view.gone()
                this.fillSpinners()
                this.resetComponents()
            } else {
                this.frf2_progress_bar.show()
                this.frf2_sending_header_text_view.show()
                this.frf2_scroll_view.gone()
                this.frf2_back_button.gone()
            }
        } else {
            this.frf2_progress_bar.show()
            this.frf2_sending_header_text_view.gone()
            this.frf2_scroll_view.gone()
            this.frf2_back_button.gone()
        }
    }

    override fun onManualAndReportGearsFinished() {
        if (this.activity == null) {
            return
        }
        this.start()
    }

    private fun refreshUncompletedReport() {
        val report = tapplication.workerReportInfoController.entityReport!!.report
        report.endWorkTime = this.frf2_end_time_edit_text.getInputText()
        report.hoursWorked = this.frf2_hours_worked_edit_text.getInputText()
        report.fuelVolumeAfter = this.frf2_fuel_volume_edit_text.getInputText()
        if (report.isOdometr.toBoolean()) {
            report.odometrValueAfter = this.frf2_odometr_edit_text.getInputText()
            report.counterValueAfter = ""
        } else {
            report.counterValueAfter = this.frf2_odometr_edit_text.getInputText()
            report.odometrValueAfter = ""
        }
        report.workScopePerformed = this.frf2_scope_of_work_edit_text.getInputText()
        report.comment = this.frf2_comment_edit_text.getInputText()
        report.unit = this.frf2_unit_of_measurement_edit_text.text.toString()
        report.amount = this.frf2_num_of_units_edit_text.getInputText()
        report.stayTimeAmount = this.frf2_idle_hours_edit_text.getInputText()
        report.stayCauser = this.frf2_causer_of_downtime_edit_text.getSelectedValue()
        report.stayReason = this.frf2_cause_of_downtime_edit_text.getInputText()
        report.generalComment = this.frf2_general_comment_edit_text.getInputText()
        presenter.groundReport2()
    }

    /**
     * Checks uncompleted report in repository.
     */

    private fun isValidReport(): Boolean {
        val report = tapplication.workerReportInfoController.entityReport!!.report
        //Hide errors:
        this.hideErrors()
        //End work time:
        return this.isValidParam(
            report.endWorkTime,
            this.frf2_end_time_error_text_view,
            this.frf2_end_time_header_text_view
        ) { report.isValidEndTime() }
                && //Hours worked:
                this.isValidParam(
                    report.hoursWorked,
                    this.frf2_hours_worked_error_text_view,
                    this.frf2_hours_worked_header_text_view
                ) { it == OregoTimeUtils.calcTimeDiff(report.beginWorkTime, report.endWorkTime) }
                && //Fuel volume after work:
                this.isValidParam(
                    report.fuelVolumeAfter,
                    this.frf2_fuel_volume_error_text_view,
                    this.frf2_fuel_volume_header_text_view
                ) { it.isNumber() }
                && //Fuel image bytes:
                this.isValidParam(
                    report.fuelImageBytesAfter,
                    this.frf2_fuel_photo_error_text_view,
                    this.frf2_fuel_photo_image_view
                )
                && //Odometr value after work:
                (odometrOrCounter())
                && //Work scope performed:
                this.isValidParam(
                    report.workScopePerformed,
                    this.frf2_scope_of_work_error_text_view,
                    this.frf2_scope_of_work_header_text_view
                )
                && //Measure unit:
                this.isValidParam(
                    report.unit,
                    this.frf2_unit_of_measurement_error_text_view,
                    this.frf2_unit_of_measurement_header_text_view
                )
                && //Work amount:
                this.isValidParam(
                    report.amount,
                    this.frf2_num_of_units_error_text_view,
                    this.frf2_num_of_units_header_text_view
                )
    }

    private fun odometrOrCounter(): Boolean {
        val report = tapplication.workerReportInfoController.entityReport!!.report
        if (report.isOdometr.toBoolean()) {
            return (this.isValidParam(
                report.odometrImageBytesAfter,
                this.frf2_odometr_photo_error_text_view,
                this.frf2_odometr_photo_image_view
            ) && this.isValidParam(
                report.odometrValueAfter,
                this.frf2_odometr_error_text_view,
                this.frf2_odometr_header_text_view
            ))
        } else {
            return (this.isValidParam(
                report.counterImageBytesAfter,
                this.frf2_odometr_photo_error_text_view,
                this.frf2_odometr_photo_image_view
            ) && this.isValidParam(
                report.counterValueAfter,
                this.frf2_odometr_error_text_view,
                this.frf2_odometr_header_text_view
            ))
        }
    }

    private fun isValidParam(
        param: String,
        error: View,
        header: View,
        isValid: (String) -> Boolean = { it.isNotEmpty() }
    ): Boolean {
        if (!isValid(param)) {
            error.show()
            this.frf2_scroll_view.post {
                this.frf2_scroll_view.scrollTo(0, header.top)
            }
            return false
        }
        return true
    }

    private fun TReport.isValidEndTime(): Boolean {
        val endWorkTime = this.endWorkTime
        return (OregoTimeUtils.isValidDate(endWorkTime, "HH:mm")
                && OregoTimeUtils.calcTimeDiff(this.beginWorkTime, endWorkTime).isNotEmpty())
    }

    /**
     * Reset ui components of the fragment.
     */

    private fun resetComponents() {
        this.clearInputTexts()
        this.hideErrors()
    }

    private fun clearInputTexts() {

        val report = tapplication.workerReportInfoController.entityReport!!.report
        val currentTime = OregoTimeUtils.getCurrentTime()
        val timeDiff = OregoTimeUtils.calcTimeDiff(report.beginWorkTime, currentTime)
        val resultTime = if (timeDiff.isNotEmpty()) timeDiff else "00:00"
        this.frf2_end_time_edit_text.setText(currentTime)
        frf2_end_time_edit_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val timeDiff2 = OregoTimeUtils.calcTimeDiff(report.beginWorkTime, s.toString())
                val resultTime2 = if (timeDiff2.isNotEmpty()) timeDiff2 else "00:00"
                frf2_hours_worked_edit_text.setText(resultTime2)
            }

        })

        this.frf2_hours_worked_edit_text.setText(resultTime)

        val uncompletedReport = tapplication.workerReportInfoController.entityReport!!.report

        if (uncompletedReport.isOdometr.toBoolean()) {
            odometerClick()
            this.frf2_odometr_edit_text.setText(uncompletedReport.odometrValueAfter)
        } else {
            counterClick()
            this.frf2_odometr_edit_text.setText(uncompletedReport.counterValueAfter)
        }
        this.frf2_fuel_volume_edit_text.setText(uncompletedReport.fuelVolumeAfter)
        this.frf2_odometr_edit_text.setText(uncompletedReport.odometrValueAfter)
        this.frf2_scope_of_work_edit_text.setText(uncompletedReport.workScopePerformed)
        this.frf2_comment_edit_text.setText(uncompletedReport.comment)
        this.frf2_num_of_units_edit_text.setText(uncompletedReport.amount)
        this.frf2_idle_hours_edit_text.setText(uncompletedReport.stayTimeAmount)
        this.frf2_cause_of_downtime_edit_text.setText(uncompletedReport.stayReason)
        this.frf2_general_comment_edit_text.setText(uncompletedReport.generalComment)

        this.frf2_unit_of_measurement_edit_text.setText(uncompletedReport.unit)
        this.frf2_causer_of_downtime_edit_text.setSelectionBy(uncompletedReport.stayCauser)

        //Photo:
        if (uncompletedReport.fuelImageBytesAfter.isNotEmpty())
            this.frf2_fuel_photo_image_view.setImageBitmap(uncompletedReport.fuelImageBytesAfter.toBitmap())
        else
            this.frf2_fuel_photo_image_view.setImageDrawable(defaultCameraDrawable)

        if (uncompletedReport.isOdometr.toBoolean()) {
            if (uncompletedReport.odometrImageBytesAfter.isNotEmpty())
                this.frf2_odometr_photo_image_view.setImageBitmap(uncompletedReport.odometrImageBytesAfter.toBitmap())
            else
                this.frf2_odometr_photo_image_view.setImageDrawable(defaultCameraDrawable)
        } else {
            if (uncompletedReport.counterImageBytesAfter.isNotEmpty())
                this.frf2_odometr_photo_image_view.setImageBitmap(uncompletedReport.counterImageBytesAfter.toBitmap())
            else
                this.frf2_odometr_photo_image_view.setImageDrawable(defaultCameraDrawable)
        }

    }

    /**
     * Hides errors.
     */

    private fun hideErrors() {
        this.frf2_end_time_error_text_view.hide()
        this.frf2_hours_worked_error_text_view.hide()
        this.frf2_fuel_volume_error_text_view.hide()
        this.frf2_odometr_error_text_view.hide()
        this.frf2_scope_of_work_error_text_view.hide()
        this.frf2_unit_of_measurement_error_text_view.hide()
        this.frf2_num_of_units_error_text_view.hide()
        this.frf2_idle_hours_error_text_view.hide()

        //Photo errors:
        this.frf2_fuel_photo_error_text_view.hide()
        this.frf2_odometr_photo_error_text_view.hide()
    }

    private fun fillSpinners() {
        val manual = this.presenter.manualManager.currentManual
        if (manual != null) {
            initEditTextAdapter(frf2_unit_of_measurement_edit_text, manual.metrics.toList())
            frf2_causer_of_downtime_edit_text.setStrings(manual.causers.toMutableList().also { it.add(0, "Нет") })
            frf2_unit_of_measurement_edit_text.setText(manual.metrics.elementAt(0))
            frf2_causer_of_downtime_edit_text.setSelection(0)
        }
    }

    private fun Spinner.setStrings(list: MutableList<String>, callback: (String) -> Unit = {}) {
        this.configure(list, this@TReportForm2Fragment.context!!, android.R.layout.simple_spinner_item, callback)
    }


    private fun initEditTextAdapter(
        autoCompleteTextView: AutoCompleteTextView,
        list: List<String>
    ) {
        val adapter = ArrayAdapter<String>(context!!, android.R.layout.simple_list_item_1, list)
        autoCompleteTextView.setAdapter(adapter)
        autoCompleteTextView.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                autoCompleteTextView.setText("")
            }
        }
    }

    /**
     * Presenter.
     */

    inner class Presenter : TAbstractFragment.Presenter(), OregoCameraComponent, OregoToastyComponent,
        CoroutineScope {

        override val activity
            get() = this@TReportForm2Fragment.activity as TActivity

        /**
         * Report manager.
         */

        val reportManager: TRepository.WorkerSubRepository.ReportManager
            get() = this.repository.workerSubRepository.reportManager

        /**
         * Manual manager.
         */

        val manualManager: TRepository.WorkerSubRepository.ManualManager
            get() = this.repository.workerSubRepository.manualManager

        override val gearManager: TGearManager
            get() = this.application.gearManager

        /**
         * Coroutine context.
         */

        override val coroutineContext = Dispatchers.Main

        /**
         * Job.
         */

        private var postJob: Job? = null

        /**
         * Photo type.
         */

        private var photoType: PhotoType? = null

        /**
         * Stop.
         */

        //override fun onStop() {
        //    this.postJob?.cancel()
        //    this.postJob = null
        //}

        /**
         * Takes photo.
         */

        fun takePhoto(photoType: PhotoType) {
            this.photoType = photoType
            this.takePhoto()
        }

        /**
         * Sends report.
         */

        fun sendReport() {
            val isSending = tapplication.workerReportInfoController.entityReport!!.isSending
            if (!isSending) {
                this@TReportForm2Fragment.refreshUncompletedReport()
                if (this@TReportForm2Fragment.isValidReport()) {
                    var job = this.postJob
                    if (job == null || !job.isActive) {
                        job = this.startSendingReport()
                        job.invokeOnCompletion {

                            val fragment = this@TReportForm2Fragment

                            val scrollView = fragment.frf2_scroll_view
                            val progressBar = fragment.frf2_progress_bar
                            val sendingStatusTextView = fragment.frf2_sending_header_text_view
                            val backButton = fragment.frf2_back_button

                            //Show ui components:
                            scrollView.show()
                            backButton.show()

                            //Hide ui components:
                            progressBar.gone()
                            sendingStatusTextView.gone()

                            //Reset job:
                            this@Presenter.postJob = null

                            GlobalScope.launch {


                                val calendar = Calendar.getInstance()
                                calendar.timeInMillis = System.currentTimeMillis()
                                val time = calendar.time
                                val year = SimpleDateFormat("yyyy").format(time).toInt()
                                val month = SimpleDateFormat("MM").format(time).toInt()
                                val day = SimpleDateFormat("dd").format(time).toInt()

                            }.invokeOnCompletion {

                                //Go to menu fragment:
                                presenter.openFragment(TWorkerReportsFragment::class.java)
                            }
                        }
                    }
                }
            }
        }

        private fun startSendingReport(): Job {
            val reportManager = this.reportManager
            val fragment = this@TReportForm2Fragment
            val presenter = this
            //Get ui components:
            val scrollView = fragment.frf2_scroll_view
            val progressBar = fragment.frf2_progress_bar
            val sendingStatusTextView = fragment.frf2_sending_header_text_view
            val backButton = fragment.frf2_back_button

            //Hide ui components:
            scrollView.gone()
            backButton.gone()

            //Show ui components:
            progressBar.show()
            sendingStatusTextView.show()

            //Start job:
            return this.launch {

                //Send report:
                val response = reportManager.sendCompletedReport(tapplication.workerReportInfoController)

                //Handle response:
                when (response) {
                    TWorkerClient.ReportController.ResponseType.NOT_SIGNED_ACCOUNT -> {
                        presenter.apply {
                            this.makeErrorToasty(this.context.getString(R.string.sending_report_by_undefined_user))
                            this.repository.workerSubRepository.signOut()
                            this.openFragment(TWorkerSignInFragment::class.java)
                        }
                    }
                    TWorkerClient.ReportController.ResponseType.NO_CONNECTION -> {
                        presenter.makeErrorToasty(fragment.getString(R.string.connection_is_failed))
                    }
                    TWorkerClient.ReportController.ResponseType.BAD_RESPONSE -> {
                        presenter.makeErrorToasty(fragment.getString(R.string.error_was_occurred))
                    }
                    TWorkerClient.ReportController.ResponseType.SUCCESS -> {
                        presenter.makeSuccessToasty(fragment.getString(R.string.report_has_sent))
                    }
                }
            }
        }

        override fun onStop() {
            super.onStop()
            this.save()
        }

        /**
         * Returns a bitmap after taking photo.
         */

        override fun onTakePhoto(bitmap: Bitmap?) {
            if (bitmap == null){
                Toasty.error(context, "Ошибка камеры").show()
                return
            }
            val fragment = this@TReportForm2Fragment
            val currentReport = tapplication.workerReportInfoController.entityReport!!.report
            val imageView =
                when (this.photoType) {
                    PhotoType.FUEL -> {
                        currentReport.fuelImageBytesAfter = bitmap.toStringBytes()
                        fragment.frf2_fuel_photo_image_view
                    }
                    PhotoType.ODOMETR -> {
                        if (currentReport.isOdometr.toBoolean()) {
                            currentReport.odometrImageBytesAfter = bitmap.toStringBytes()
                            currentReport.counterImageBytesAfter = ""
                        } else {
                            currentReport.counterImageBytesAfter = bitmap.toStringBytes()
                            currentReport.odometrImageBytesAfter = ""
                        }
                        fragment.frf2_odometr_photo_image_view
                    }
                    null -> null
                }
            imageView?.setImageBitmap(bitmap)
        }

        /**
         * Callback.
         */

        override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
        ) {
            this.onCameraRequestPermissionsResult(requestCode, permissions, grantResults)
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            this.onCameraActivityResult(requestCode, resultCode, data)
        }

        override fun onBackPressed(): Boolean {
            this.openFragment(TWorkerReportsFragment::class.java)
            return true
        }

        fun save() {
            this@TReportForm2Fragment.refreshUncompletedReport()
        }

        fun groundReport2() {
            launch {
                reportManager.groundReport2(tapplication.workerReportInfoController.entityReport!!)
            }
        }
    }
}