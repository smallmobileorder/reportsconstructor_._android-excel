package com.orego.truckapp.ui.fragment.implementation.worker

import android.annotation.SuppressLint
import com.orego.truckapp.R
import com.orego.truckapp.api.util.common.bitmap.toBitmap
import com.orego.truckapp.api.util.ui.component.OregoToastyComponent
import com.orego.truckapp.ui.controller.TUiWorkerReportInfoController
import com.orego.truckapp.ui.fragment.TAbstractFragment
import kotlinx.android.synthetic.main.fragment_worker_report_info.*

class TWorkerReportInfoFragment : TAbstractFragment(R.layout.fragment_worker_report_info) {

    override val presenter = Presenter()

    private val reportController: TUiWorkerReportInfoController
        get () = this.presenter.application.workerReportInfoController

    override fun onFirstViewCreated() {
        this.configureListeners()
    }

    private fun configureListeners() {
        this.fwri_back_button.setOnClickListener {
            this.onBackPressed()
        }
    }

    /**
     * Start.
     */

    override fun onStart() {
        super.onStart()
        this.fillComponents()
    }

    @SuppressLint("SetTextI18n")
    private fun fillComponents() {
        val displayedReportEntity = this.reportController.entityReport
        if (displayedReportEntity == null) {
            this.presenter.makeErrorToasty(this.getString(R.string.cannot_open_current_report))
            return
        }
        val displayedReport = displayedReportEntity.report
        //Report:
        this.fwri_report_header_text_view.text = "Рапорт " + displayedReport.number
        //Date:
        this.fwri_date_header_text_view.text = "Дата: " + displayedReport.date
        //Person:
        this.fwri_name_header_text_view.text = "Заполнил: " + displayedReport.personName
        //Post:
        this.fwri_position_header_text_view.text = "Должность: " + displayedReport.post
        //Executor:
        this.fwri_organization_header_text_view.text =
            "Организация, предоставляющая услугу: " + displayedReport.organization
        //Customer:
        this.fwri_customer_header_text_view.text = "Заказчик: " + displayedReport.customer
        //Car:
        this.fwri_info_car_header_text_view.text = "Машина: " + displayedReport.carInfo
        //Driver:
        this.fwri_driver_header_text_view.text = "Машинист/водитель: " + displayedReport.driver
        //Column number:
        this.fwri_column_number_header_text_view.text = "Номер колонны: " + displayedReport.columnNumber
        //Site number:
        this.fwri_site_number_header_text_view.text = "Номер участка: " + displayedReport.siteNumber
        //Object groupName:
        this.fwri_area_name_header_text_view.text =
            "Наименование и адрес объекта: " + displayedReport.objectNameAndAddress
        //Fuel before:
        this.fwri_fuel_before_header_text_view.text =
            "Наличие горючего в начале смены: " + displayedReport.fuelVolumeBefore
        this.fwri_fuel_before_image_view.setImageBitmap(displayedReport.fuelImageBytesBefore.toBitmap())
        //Fuel type:
        this.fwri_fuel_type_header_text_view.text = "Вид горючего: " + displayedReport.fuelType
        //Amount of fuel issued:
        this.fwri_fuel_amount_header_text_view.text = "Выдано горючего: " + displayedReport.amountOfFuelIssued
        if (displayedReport.isOdometr.toBoolean()) {
            //Odometr before:
            this.fwri_odometr_before_header_text_view.text =
                "Показание одометра на начало работ: " + displayedReport.odometrValueBefore
            this.fwri_odometr_before_image_view.setImageBitmap(displayedReport.odometrImageBytesBefore.toBitmap())
        } else {
            //Counter before:
            this.fwri_odometr_before_header_text_view.text =
                "Показания счетчика моточасов на начало работ: " + displayedReport.counterValueBefore
            this.fwri_odometr_before_image_view.setImageBitmap(displayedReport.counterImageBytesBefore.toBitmap())
        }
        this.fwri_start_time_work_header_text_view.text =
            "Время начала работ на объекте: " + displayedReport.beginWorkTime
        //End time:
        this.fwri_end_time_header_text_view.text = "Время окончания работ на объекте: " + displayedReport.endWorkTime
        //Hours worked:
        this.fwri_hours_worked_header_text_view.text = "Отработано часов: " + displayedReport.hoursWorked
        //Fuel after:
        this.fwri_fuel_after_header_text_view.text =
            "Наличие горючего в конце смены: " + displayedReport.fuelVolumeAfter
        this.fwri_fuel_after_image_view.setImageBitmap(displayedReport.fuelImageBytesAfter.toBitmap())
        if (displayedReport.isOdometr.toBoolean()) {
            //Odometr after:
            this.fwri_odometr_after_header_text_view.text =
                "Показания одометра по окончанию работ: " + displayedReport.odometrValueAfter
            this.fwri_odometr_after_image_view.setImageBitmap(displayedReport.odometrImageBytesAfter.toBitmap())
        }else {
            //Counter:
            this.fwri_odometr_after_header_text_view.text =
                "Показания счетчика моточасов по окончанию работ: " + displayedReport.counterValueAfter
            this.fwri_odometr_after_image_view.setImageBitmap(displayedReport.counterImageBytesAfter.toBitmap())
        }
        this.fwri_scope_of_work_header_text_view.text = "Объём выполненных работ: " + displayedReport.workScopePerformed
        //Measure unit:
        this.fwri_unit_of_measurement_header_text_view.text = "Единица измерения: " + displayedReport.unit
        //Unit amount:
        this.fwri_num_of_units_header_text_view.text = "Количество: " + displayedReport.amount
        //Comment:
        this.fwri_comment_header_text_view.text = "Коментарий:" + displayedReport.comment
        //Stay hours:
        this.fwri_idle_hours_header_text_view.text = "Количество часов простоя: " + displayedReport.stayTimeAmount
        //Causer:
        this.fwri_causer_of_downtime_header_text_view.text = "Виновник простоя: " + displayedReport.stayCauser
        //Reason:
        this.fwri_cause_of_downtime_header_text_view.text = "Причины простоя: " + displayedReport.stayReason
        //General comment:
        this.fwri_general_comment_header_text_view.text = "Общий комментарий: " + displayedReport.generalComment
    }

    /**
     * Presenter.
     */

    inner class Presenter : TAbstractFragment.Presenter(), OregoToastyComponent {

        override fun onBackPressed(): Boolean {
            this.openFragment(TWorkerReportsFragment::class.java)
            return true
        }
    }
}