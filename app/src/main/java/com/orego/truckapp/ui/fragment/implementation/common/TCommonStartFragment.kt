package com.orego.truckapp.ui.fragment.implementation.common

import com.orego.truckapp.R
import com.orego.truckapp.ui.fragment.TAbstractFragment
import com.orego.truckapp.ui.fragment.implementation.admin.TAdminSignInFragment
import com.orego.truckapp.ui.fragment.implementation.worker.TWorkerSignInFragment
import kotlinx.android.synthetic.main.fragment_common_start.*

class TCommonStartFragment : TAbstractFragment(R.layout.fragment_common_start) {

    override val presenter = Presenter()

    override fun onFirstViewCreated() {
        this.configureListeners()
    }

    private fun configureListeners() {
        this.fcs_sign_in_as_admin_button.setOnClickListener {
            this.presenter.signInAsAdmin()
        }
        this.fcs_sign_in_as_worker_button.setOnClickListener {
            this.presenter.signInAsWorker()
        }
    }

    /**
     * Presenter.
     */

    inner class Presenter : TAbstractFragment.Presenter() {

        fun signInAsAdmin() {
            this.openFragment(TAdminSignInFragment::class.java)
        }

        fun signInAsWorker() {
            this.openFragment(TWorkerSignInFragment::class.java)
        }
    }
}