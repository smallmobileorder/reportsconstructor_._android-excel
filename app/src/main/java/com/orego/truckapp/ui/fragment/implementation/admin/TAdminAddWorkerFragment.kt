package com.orego.truckapp.ui.fragment.implementation.admin

import com.orego.truckapp.R
import com.orego.truckapp.api.repo.gearManager.OregoGearManager
import com.orego.truckapp.api.util.common.account.OregoAccountUtils
import com.orego.truckapp.api.util.ui.view.editText.clearInputText
import com.orego.truckapp.api.util.ui.view.editText.getInputText
import com.orego.truckapp.api.util.ui.view.gone
import com.orego.truckapp.api.util.ui.view.hide
import com.orego.truckapp.api.util.ui.view.show
import com.orego.truckapp.repo.client.TAdminClient
import com.orego.truckapp.repo.client.model.admin.group.worker.TWorker
import com.orego.truckapp.repo.gearManager.TGearManager
import com.orego.truckapp.ui.fragment.TAbstractFragment
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_admin_add_worker.*

class TAdminAddWorkerFragment : TAbstractFragment(R.layout.fragment_admin_add_worker) {

    override val presenter = Presenter()

    override fun onFirstViewCreated() {
        this.faaw_back_button.setOnClickListener {
            this.onBackPressed()
        }
        this.faaw_create_a_worker_button.setOnClickListener {
            this.presenter.createWorker()
        }
    }

    override fun onStart() {
        super.onStart()
        this.resetComponents()
    }

    private fun resetComponents() {
        this.scrollToTop()
        this.clearInputText()
        this.hideErrors()
        this.setInputMode()
    }

    private fun scrollToTop() {
        this.faaw_scroll_view.scrollY = 0
    }

    private fun hideErrors() {
        this.faaw_login_error_text_view.hide()
        this.faaw_name_error_text_view.hide()
        this.faaw_password_error_text_view.hide()
        this.faaw_phone_error_text_view.hide()
        this.faaw_post_error_text_view.hide()
    }

    private fun clearInputText() {
        this.faaw_login_edit_text.clearInputText()
        this.faaw_name_edit_text.clearInputText()
        this.faaw_password_edit_text.clearInputText()
        this.faaw_phone_edit_text.clearInputText()
        this.faaw_post_edit_text.clearInputText()
    }

    /**
     * Input mode.
     */

    private fun setInputMode() {
        this.faaw_back_button.show()
        this.faaw_loading_constraint_layout.gone()
    }

    /**
     * Loading mode.
     */

    private fun setLoadingMode() {
        this.faaw_back_button.gone()
        this.faaw_loading_constraint_layout.show()
    }

    /**
     * Presenter.
     */

    inner class Presenter : TAbstractFragment.Presenter() {

        override fun onBackPressed(): Boolean {
            this.gearManager.cancelGear(TGearManager.AdminPostWorkerGear::class.java)
            this.openFragment(TAdminWorkersFragment::class.java)
            return true
        }

        fun createWorker() {
            val fragment = this@TAdminAddWorkerFragment
            fragment.hideErrors()
            //Login:
            val loginEditText = fragment.faaw_login_edit_text
            val login = loginEditText.getInputText()
            if (!OregoAccountUtils.isCorrectLogin(login)) {
                val errorTextView = fragment.faaw_login_error_text_view
                errorTextView.show()
                return
            }
            //Name:
            val nameEditText = fragment.faaw_name_edit_text
            val personName = nameEditText.getInputText()
            if (personName == "") {
                val errorTextView = fragment.faaw_name_error_text_view
                errorTextView.show()
                return
            }
            //Password:
            val passwordEditText = fragment.faaw_password_edit_text
            val password = passwordEditText.getInputText()
            if (password == "") {
                val errorTextView = fragment.faaw_password_error_text_view
                errorTextView.show()
                return
            }
            //Phone:
            val phoneEditText = fragment.faaw_phone_edit_text
            val phone = phoneEditText.getInputText()

            //Post:
            val postInputText = fragment.faaw_post_edit_text
            val post = postInputText.getInputText()


            //Get workersUrl:
            val currentGroup = this.application.adminGroupInfoController.currentGroup ?: return
            val workersUrl = currentGroup.workersUrl
            val manualUrl = currentGroup.manualUrl
            //Build worker info:
            val worker = TWorker(
                login = login,
                password = password,
                name = personName,
                post = post,
                phone = phone,
                manualUrl = manualUrl
            )
            //Compose params:
            val workerParams = WorkerParams(worker, workersUrl)
            this@TAdminAddWorkerFragment.setLoadingMode()
            this.gearManager.launchGear(
                TGearManager.AdminPostWorkerGear::class.java, workerParams
            )
        }

        override fun onGearFinished(gearClazz: Class<out OregoGearManager.Gear>, result: Any?) {
            if (gearClazz != TGearManager.AdminPostWorkerGear::class.java) {
                return
            }
            if (result !is Pair<*, *>) {
                return
            }
            val type = result.first as? TAdminClient.WorkerController.ResponseType ?: return
            val context = this.context
            when (type) {
                TAdminClient.WorkerController.ResponseType.IS_EXIST -> {
                    setInputMode()
                    Toasty.error(context, "Участник с таким логином в этой группе уже существует").show()
                }
                TAdminClient.WorkerController.ResponseType.BAD_RESPONSE -> {
                    setInputMode()
                    Toasty.error(context, "Сервис недоступен").show()
                }
                TAdminClient.WorkerController.ResponseType.NO_CONNECTION -> {
                    setInputMode()
                    Toasty.error(context, "Отсутствует соедиенение с интернетом").show()
                }
                TAdminClient.WorkerController.ResponseType.SUCCESS -> {
                    Toasty.success(context, "Участник успешно создан").show()
                    this@TAdminAddWorkerFragment.resetComponents()
                    this.application.adminWorkerInfoController.currentWorker = null
                    this.openFragment(TAdminWorkersFragment::class.java)
                }
            }
        }
    }

    data class WorkerParams(
        val worker: TWorker,
        val workersUrl: String
    )
}
