package com.orego.truckapp.ui.util.common

import com.orego.truckapp.api.util.common.string.isNumber

fun String.matchesReport(): Boolean {
    val parts = this.split('/')
    if (parts.size != 2) {
        return false
    }
    if (!parts[0].matches("№\\d+".toRegex())) {
        return false
    }

    return true
}

fun String.matchesReportDate():Boolean{
    val parts = this.split('/')
    if (parts.size != 3) {
        return false
    }
    if (!parts[0].isNumber() && !parts[1].isNumber() && !parts[2].isNumber()) {
        return false
    }
    return true
}
