package com.orego.truckapp.ui.fragment.implementation.worker

import com.orego.truckapp.R
import com.orego.truckapp.api.repo.gearManager.OregoGearManager
import com.orego.truckapp.api.util.ui.component.OregoToastyComponent
import com.orego.truckapp.api.util.ui.view.editText.clearInputText
import com.orego.truckapp.api.util.ui.view.editText.getInputText
import com.orego.truckapp.api.util.ui.view.gone
import com.orego.truckapp.api.util.ui.view.hide
import com.orego.truckapp.api.util.ui.view.show
import com.orego.truckapp.repo.TRepository
import com.orego.truckapp.repo.client.TWorkerClient
import com.orego.truckapp.repo.gearManager.TGearManager
import com.orego.truckapp.ui.fragment.TAbstractFragment
import com.orego.truckapp.ui.fragment.implementation.common.TCommonStartFragment
import kotlinx.android.synthetic.main.fragment_worker_sign_in.*
import kotlinx.coroutines.*

class TWorkerSignInFragment : TAbstractFragment(R.layout.fragment_worker_sign_in) {

    /**
     * Presenter.
     */

    override val presenter = Presenter()

    /**
     * Components.
     */

    private val loginErrorTextView by lazy {
        this.fwsi_login_error_text_view
    }

    override fun onFirstViewCreated() {
        this.configureListeners()
        this.tryAutoSignIn()
    }

    private fun configureListeners() {
        this.fwsi_sign_in_button.setOnClickListener {
            this.presenter.signIn()
        }
        this.fwsi_back_button.setOnClickListener {
            this.onBackPressed()
        }
    }

    private fun tryAutoSignIn() {
        this.presenter.tryAutoSignIn()
    }

    /**
     * Component management.
     */

    private fun buildAuthParams(): AuthParams? {
        //Hide error:
        this.fwsi_login_error_text_view.hide()
        //Start collect data:
        val login = this.fwsi_login_edit_text.getInputText()
        val adminEmail = this.fwsi_admin_id_edit_text.getInputText()
        val groupName = this.fwsi_group_id_edit_text.getInputText()
        val password = this.fwsi_password_edit_text.getInputText()
        if (login.isEmpty() || adminEmail.isEmpty() || groupName.isEmpty() || password.isEmpty()) {
            this.fwsi_login_error_text_view.show()
            return null
        }
        return AuthParams(login, password, adminEmail, groupName)
    }

    private fun resetComponents() {
        this.loginErrorTextView.hide()
        this.fwsi_progress_bar.gone()
        this.clearInputTexts()
        this.setInputMode()
    }

    private fun clearInputTexts() {
        this.fwsi_login_edit_text.clearInputText()
        this.fwsi_password_edit_text.clearInputText()
    }

    /**
     * Ui modes.
     */

    private fun setLoadingMode() {
        //Enable sign in button:
        this.fwsi_sign_in_button.gone()
        //Hide progress bar:
        this.fwsi_progress_bar.show()
    }

    private fun setInputMode() {
        //Enable sign in button:
        this.fwsi_sign_in_button.show()
        //Hide progress bar:
        this.fwsi_progress_bar.gone()
    }

    /**
     * Presenter.
     */

    inner class Presenter : TAbstractFragment.Presenter(), CoroutineScope, OregoToastyComponent {

        /**
         * Coroutine context.
         */

        override val coroutineContext = Dispatchers.Main

        /**
         * Worker manager.
         */

        private val workerSubRepository: TRepository.WorkerSubRepository
            get() = this.repository.workerSubRepository

        /**
         * Stop.
         */

        override fun onStop() {
            this.gearManager.cancelGear(TGearManager.WorkerSignInGear::class.java)
            //Show sign in button:
            this@TWorkerSignInFragment.setInputMode() //FIXED!
        }

        /**
         * Tries to sign in by cached last worker data.
         */

        fun tryAutoSignIn() {
            //Lock input components:
            this@TWorkerSignInFragment.setLoadingMode()
            //Check auto sign in:
            if (this.workerSubRepository.accountManager.tryAutoSignIn()) {
                this.beginSession()
            } else {
                this@TWorkerSignInFragment.setInputMode()
            }
        }

        /**
         * Sign in.
         */

        fun signIn() {
            val params = this@TWorkerSignInFragment.buildAuthParams() ?: return
            //Show loading:
            this@TWorkerSignInFragment.setLoadingMode()
            //Sign in task:
            this.gearManager.launchGear(TGearManager.WorkerSignInGear::class.java, params)
        }

        /**
         * On sign in task finished.
         */

        override fun onGearFinished(gearClazz: Class<out OregoGearManager.Gear>, result: Any?) {
            if (gearClazz == TGearManager.WorkerSignInGear::class.java
                && result is TWorkerClient.AuthController.ResponseType
            ) {
                //Handle response:
                when (result) {
                    TWorkerClient.AuthController.ResponseType.INVALID_AUTH_PARAMS -> {
                        this.stopWithError(this@TWorkerSignInFragment.getString(R.string.invalid_login_or_password))
                    }
                    TWorkerClient.AuthController.ResponseType.BAD_RESPONSE -> {
                        this.stopWithError(this@TWorkerSignInFragment.getString(R.string.conflict_server))
                    }
                    TWorkerClient.AuthController.ResponseType.NO_CONNECTION -> {
                        this.stopWithError(this@TWorkerSignInFragment.getString(R.string.connection_is_failed))
                    }
                    TWorkerClient.AuthController.ResponseType.SUCCESS -> {
                        this.beginSession()
                    }
                }
            }
        }

        private fun stopWithError(message: String) {
            this.makeErrorToasty(message)
            this@TWorkerSignInFragment.setInputMode()
        }

        /**
         * Starts user session.
         */

        private fun beginSession() {

            //Clear fragments for current user:
            this.recycleFragment(TReportForm1Fragment())
            this.recycleFragment(TReportForm2Fragment())
            this.recycleFragment(TWorkerReportsFragment())

            val reportManager = this.workerSubRepository.reportManager

            //Load progress of uncompleted report:
            reportManager.digReportProgress()

            //Load report materials for work:
            this.gearManager.launchGear(TGearManager.WorkerFetchReportsGear::class.java)

            //Load manual materials for work:
            this.gearManager.launchGear(TGearManager.WorkerFetchManualGear::class.java)

            //Clear fields on fragment:
            this@TWorkerSignInFragment.resetComponents()

            //Open menu fragment:
            this.openFragment(TWorkerReportsFragment::class.java)
        }
        override fun onBackPressed(): Boolean {
            this@TWorkerSignInFragment.setInputMode()
            this.gearManager.cancelGear(TGearManager.WorkerSignInGear::class.java)
            this.openFragment(TCommonStartFragment::class.java)
            return true
        }
    }

    /**
     * Auth params.
     */

    data class AuthParams(
        val login: String,
        val password: String,
        val adminEmail: String,
        val groupName: String
    )
}