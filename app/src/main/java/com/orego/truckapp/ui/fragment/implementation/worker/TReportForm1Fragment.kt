package com.orego.truckapp.ui.fragment.implementation.worker

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.widget.doOnTextChanged
import com.orego.truckapp.R
import com.orego.truckapp.api.util.common.OregoTimeUtils
import com.orego.truckapp.api.util.common.bitmap.toBitmap
import com.orego.truckapp.api.util.common.bitmap.toStringBytes
import com.orego.truckapp.api.util.common.string.isNumber
import com.orego.truckapp.api.util.ui.component.OregoCameraComponent
import com.orego.truckapp.api.util.ui.component.OregoToastyComponent
import com.orego.truckapp.api.util.ui.view.editText.clearInputText
import com.orego.truckapp.api.util.ui.view.editText.getInputText
import com.orego.truckapp.api.util.ui.view.gone
import com.orego.truckapp.api.util.ui.view.hide
import com.orego.truckapp.api.util.ui.view.show
import com.orego.truckapp.api.util.ui.view.spinner.configure
import com.orego.truckapp.api.util.ui.view.spinner.getSelectedValue
import com.orego.truckapp.api.util.ui.view.spinner.setSelectionBy
import com.orego.truckapp.core.TActivity
import com.orego.truckapp.repo.TRepository
import com.orego.truckapp.repo.client.model.admin.group.worker.report.TReport
import com.orego.truckapp.repo.gearManager.TGearManager
import com.orego.truckapp.ui.fragment.TAbstractFragment
import com.orego.truckapp.ui.util.common.matchesReport
import com.orego.truckapp.ui.util.photoType.PhotoType
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_report_form_1.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*


class TReportForm1Fragment : TAbstractFragment(R.layout.fragment_report_form_1) {

    /**
     * Presenter.
     */

    override val presenter = Presenter()

    /**
     * Default image.
     */

    private val defaultCameraDrawable by lazy {
        this@TReportForm1Fragment.resources.getDrawable(R.drawable.ic_camera)
    }

    /**
     * Flag for cleaning user ui components.
     */

    override fun onFirstViewCreated() {
        this.configureListeners()
        this.hideErrorsConfiguration()
    }

    private fun hideErrorsConfiguration() {
        this.hideListenerErrorEditText(frf1_report_edit_text, frf1_report_error_text_view)
        this.hideListenerErrorEditText(frf1_date_edit_text, frf1_date_error_text_view)
        this.hideListenerErrorEditText(frf1_organization_edit_text, frf1_organization_error_text_view)
        this.hideListenerErrorEditText(frf1_customer_edit_text, frf1_customer_error_text_view)
        this.hideListenerErrorEditText(frf1_info_car_edit_text, frf1_info_car_error_text_view)
        this.hideListenerErrorEditText(frf1_driver_edit_text, frf1_driver_error_text_view)
        this.hideListenerErrorEditText(frf1_column_number_edit_text, frf1_column_number_error_text_view)
        this.hideListenerErrorEditText(frf1_site_number_edit_text, frf1_report_error_text_view)
        this.hideListenerErrorEditText(frf1_area_name_edit_text, frf1_area_name_error_text_view)
        this.hideListenerErrorEditText(frf1_fuel_amount_edit_text, frf1_fuel_amount_error_text_view)
        this.hideListenerErrorEditText(frf1_fuel_volume_edit_text, frf1_fuel_volume_error_text_view)
        this.hideListenerErrorEditText(frf1_start_time_odometr_edit_text, frf1_start_time_odometr_error_text_view)
    }

    private fun hideListenerErrorEditText(editText: EditText, textView: TextView) {
        editText.doOnTextChanged { _, _, _, _ ->
            textView.hide()
        }
    }

    private fun hideListenerErrorCamera(textView: TextView) {
        textView.hide()
    }

    private fun configureListeners() {
        this.frf1_take_fuel_photo_button.setOnClickListener {
            this.presenter.takePhoto(PhotoType.FUEL)
        }
        this.frf1_take_odometr_photo_button.setOnClickListener {
            this.presenter.takePhoto(PhotoType.ODOMETR)
        }
        this.frf1_back_button.setOnClickListener {
            this.onBackPressed()
        }
        this.frf1_finish_button.setOnClickListener {
            this.presenter.finish()
        }

        this.odometer_active.setOnClickListener {
            this.odometerClick()
        }
        this.counter_active.setOnClickListener {
            this.counterClick()
        }
        this.frf1_report_edit_text.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                var number = this.frf1_report_edit_text.text.toString()
                if (number.isEmpty()) number = "${this.presenter.getReportCount() + 1}"
                val calendar = Calendar.getInstance()
                calendar.timeInMillis = System.currentTimeMillis()
                val time = calendar.time
                val year = SimpleDateFormat("yyyy").format(time)
                val month = SimpleDateFormat("MM").format(time)
                val day = SimpleDateFormat("dd").format(time)
                val refactMonth = if (month.startsWith('0'))
                    month.substring(1)
                else month
                val resultNumberReport = "№$number/$day.$refactMonth.$year"
                this.frf1_report_edit_text.setText(resultNumberReport)
            } else {
                this.frf1_report_edit_text.clearInputText()
            }
        }
    }

    fun odometerClick() {
        val uncompletedReport = this.presenter.reportManager.uncompletedReport
        if (!uncompletedReport.isOdometr.toBoolean()) {
            uncompletedReport.counterImageBytesBefore = ""
            frf1_odometr_photo_image_view.setImageResource(R.drawable.ic_camera)
        }
        uncompletedReport.isOdometr = true.toString()
        odometer_active.setBackgroundResource(R.drawable.bg_blue_passive)
        counter_active.setBackgroundResource(R.drawable.bg_blue_active)
        frf1_start_time_odometr_header_text_view.text = getString(R.string.odometr_value_before_work)
        frf1_odometr_photo_error_text_view.text = getString(R.string.odometr_photo_is_absent)
    }

    fun counterClick() {
        val uncompletedReport = this.presenter.reportManager.uncompletedReport
        if (uncompletedReport.isOdometr.toBoolean()) {
            frf1_odometr_photo_image_view.setImageResource(R.drawable.ic_camera)
            uncompletedReport.odometrImageBytesBefore = ""
        }
        uncompletedReport.isOdometr = false.toString()
        odometer_active.setBackgroundResource(R.drawable.bg_blue_active)
        counter_active.setBackgroundResource(R.drawable.bg_blue_passive)
        frf1_start_time_odometr_header_text_view.text = getString(R.string.counter_value_before_work)
        frf1_odometr_photo_error_text_view.text = getString(R.string.counter_photo_is_absent)
    }

    /**
     * Start.
     */

    override fun onStart() {
        super.onStart()
        if (this.areGearsReady) {
            this.start()
        }
    }

    override fun onManualAndReportGearsFinished() {
        if (this.activity == null) {
            return
        }
        this.start()
    }

    private fun start() {
        this.frf1_loading_constraint_layout?.gone()
        this.fillSpinners()
        this.resetUiComponents()
    }

    /**
     * Builds a report.
     */

    private fun refreshReport() {
        val report = this.presenter.reportManager.uncompletedReport
        report.number = this.frf1_report_edit_text.getInputText()
        report.date = this.frf1_date_edit_text.getInputText()
        report.personName = this.frf1_name_edit_text.getSelectedValue()
        report.post = this.frf1_position_edit_text.getSelectedValue()
        report.organization = this.frf1_organization_edit_text.text.toString()
        report.customer = this.frf1_customer_edit_text.text.toString()
        report.carInfo = this.frf1_info_car_edit_text.text.toString()
        report.driver = this.frf1_driver_edit_text.text.toString()
        report.columnNumber = this.frf1_column_number_edit_text.text.toString()
        report.siteNumber = this.frf1_site_number_edit_text.text.toString()
        report.objectNameAndAddress = this.frf1_area_name_edit_text.text.toString()
        report.fuelVolumeBefore = this.frf1_fuel_volume_edit_text.getInputText()
        report.fuelType = this.frf1_fuel_type_edit_text.getSelectedValue()
        report.amountOfFuelIssued = this.frf1_fuel_amount_edit_text.getInputText()
        if (report.isOdometr.toBoolean()) {
            report.odometrValueBefore = this.frf1_start_time_odometr_edit_text.getInputText()
            report.counterValueBefore = ""
        } else {
            report.odometrValueBefore = ""
            report.counterValueBefore = this.frf1_start_time_odometr_edit_text.getInputText()
        }
        report.beginWorkTime = this.frf1_start_time_work_edit_text.getInputText()
        this.presenter.reportManager.groundUncompletedReport()
    }

    /**
     * Checks form 1.
     */

    private fun isValidReport(): Boolean {
        val report = this.presenter.reportManager.uncompletedReport

        //Hide errors:
        this.hideErrors()
        //Report number:
        return this.isValidParam(
            report.number,
            this.frf1_report_error_text_view,
            this.frf1_report_header_text_view
        ) { it.matchesReport() }
                && //Report date:
                this.isValidParam(
                    report.date,
                    this.frf1_date_error_text_view,
                    this.frf1_date_header_text_view
                )
                && //Person groupName:
                this.isValidParam(
                    report.personName,
                    this.frf1_name_error_text_view,
                    this.frf1_name_header_text_view
                )
                &&  //Post:
                this.isValidParam(
                    report.post,
                    this.frf1_position_error_text_view,
                    this.frf1_position_header_text_view
                )
                && //Organization:
                this.isValidParam(
                    report.organization,
                    this.frf1_organization_error_text_view,
                    this.frf1_organization_header_text_view
                )
                && //Customer:
                this.isValidParam(
                    report.customer,
                    this.frf1_customer_error_text_view,
                    this.frf1_customer_header_text_view
                )
                && //Car
                this.isValidParam(
                    report.carInfo,
                    this.frf1_info_car_error_text_view,
                    this.frf1_info_car_header_text_view
                )
                && //Driver:
                this.isValidParam(
                    report.driver,
                    this.frf1_driver_error_text_view,
                    this.frf1_driver_header_text_view
                )
                && //Column number:
                this.isValidParam(
                    report.columnNumber,
                    this.frf1_column_number_error_text_view,
                    this.frf1_column_number_header_text_view
                )
                && //Site:
                this.isValidParam(
                    report.siteNumber,
                    this.frf1_site_number_error_text_view,
                    this.frf1_site_number_header_text_view
                ) { it.isNumber() }
                && //Object address:
                this.isValidParam(
                    report.objectNameAndAddress,
                    this.frf1_area_name_error_text_view,
                    this.frf1_area_name_header_text_view
                )
                && //Fuel volume:
                this.isValidParam(
                    report.fuelVolumeBefore,
                    this.frf1_fuel_volume_error_text_view,
                    this.frf1_fuel_volume_header_text_view
                ) { it.isNumber() }
                && //Fuel photo:
                this.isValidParam(
                    report.fuelImageBytesBefore,
                    this.frf1_fuel_photo_error_text_view,
                    this.frf1_fuel_photo_image_view
                )
                && //Fuel type:
                this.isValidParam(
                    report.fuelType,
                    this.frf1_fuel_type_error_text_view,
                    this.frf1_fuel_type_header_text_view
                )
                && //Amount of fuel:
                this.isValidParam(
                    report.amountOfFuelIssued,
                    this.frf1_fuel_amount_error_text_view,
                    this.frf1_fuel_amount_header_text_view
                ) { it.isNumber() }
                && //Odometr photo:
                (odometrOrCounter())
                && //Start work time:
                this.isValidParam(
                    report.beginWorkTime,
                    this.frf1_start_time_work_error_text_view,
                    this.frf1_start_time_work_header_text_view
                ) { OregoTimeUtils.isValidDate(it, "HH:mm") }
    }

    private fun odometrOrCounter(): Boolean {
        val report = this.presenter.reportManager.uncompletedReport
        if (report.isOdometr.toBoolean()) {
            return (this.isValidParam(
                report.odometrImageBytesBefore,
                this.frf1_odometr_photo_error_text_view,
                this.frf1_odometr_photo_image_view
            ) && this.isValidParam(
                report.odometrValueBefore,
                this.frf1_start_time_odometr_error_text_view,
                this.frf1_start_time_odometr_header_text_view
            ))
        } else {
            return (this.isValidParam(
                report.counterImageBytesBefore,
                this.frf1_odometr_photo_error_text_view,
                this.frf1_odometr_photo_image_view
            ) && this.isValidParam(
                report.counterValueBefore,
                this.frf1_start_time_odometr_error_text_view,
                this.frf1_start_time_odometr_header_text_view
            ))
        }
    }

    private fun isValidParam(
        param: String,
        error: View,
        header: View,
        isValid: (String) -> Boolean = { it.isNotEmpty() }
    ): Boolean {
        if (!isValid(param)) {
            error.show()
            this.frf1_scroll_view.post {
                this.frf1_scroll_view.scrollTo(0, header.top)
            }
            return false
        }
        return true
    }


    /**
     * Clear ui components.
     */

    private fun resetUiComponents() {
        this.hideErrors()
        val reportManager = this.presenter.reportManager
        if (reportManager.progress == TRepository.WorkerSubRepository.ReportManager.Progress.FIRST_FORM_STARTED)
            this.restoreInputText()
        else
            this.clearInputTexts()
    }

    private fun restoreInputText() {
        Log.i("MY_TAG_ILYA", "clearInputText firstFormStarted")
        val reportManager = this.presenter.reportManager
        val uncompletedReport = reportManager.uncompletedReport

        //Edit text:
        this.frf1_report_edit_text.setText(uncompletedReport.number)
        this.frf1_date_edit_text.setText(uncompletedReport.date)
        this.frf1_fuel_volume_edit_text.setText(uncompletedReport.fuelVolumeBefore)
        this.frf1_fuel_amount_edit_text.setText(uncompletedReport.amountOfFuelIssued)
        if (uncompletedReport.isOdometr.toBoolean()) {
            odometerClick()
            this.frf1_start_time_odometr_edit_text.setText(uncompletedReport.odometrValueBefore)
        } else {
            counterClick()
            this.frf1_start_time_odometr_edit_text.setText(uncompletedReport.counterValueBefore)
        }
        this.frf1_start_time_work_edit_text.setText(uncompletedReport.beginWorkTime)

        //Spinner:
        this.frf1_name_edit_text.setSelectionBy(uncompletedReport.personName)
        this.frf1_position_edit_text.setSelectionBy(uncompletedReport.post)
        this.frf1_fuel_type_edit_text.setSelectionBy(uncompletedReport.fuelType)
        this.frf1_organization_edit_text.setText(uncompletedReport.organization)
        this.frf1_customer_edit_text.setText(uncompletedReport.customer)
        this.frf1_info_car_edit_text.setText(uncompletedReport.carInfo)
        this.frf1_driver_edit_text.setText(uncompletedReport.driver)
        this.frf1_column_number_edit_text.setText(uncompletedReport.columnNumber)
        this.frf1_site_number_edit_text.setText(uncompletedReport.siteNumber)
        this.frf1_area_name_edit_text.setText(uncompletedReport.objectNameAndAddress)

        //Photo:
        if (uncompletedReport.fuelImageBytesBefore.isNotEmpty())
            this.frf1_fuel_photo_image_view.setImageBitmap(uncompletedReport.fuelImageBytesBefore.toBitmap())
        else
            this.frf1_fuel_photo_image_view.setImageDrawable(defaultCameraDrawable)

        if (uncompletedReport.isOdometr.toBoolean()) {
            if (uncompletedReport.odometrImageBytesBefore.isNotEmpty())
                this.frf1_odometr_photo_image_view.setImageBitmap(uncompletedReport.odometrImageBytesBefore.toBitmap())
            else
                this.frf1_odometr_photo_image_view.setImageDrawable(defaultCameraDrawable)
        } else {

            if (uncompletedReport.counterImageBytesBefore.isNotEmpty())
                this.frf1_odometr_photo_image_view.setImageBitmap(uncompletedReport.counterImageBytesBefore.toBitmap())
            else
                this.frf1_odometr_photo_image_view.setImageDrawable(defaultCameraDrawable)
        }

    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    private fun clearInputTexts() {
        val uncompletedReport = presenter.reportManager.uncompletedReport

        Log.i("MY_TAG_ILYA", "clearInputText firstFormNotStarted")
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        val time = calendar.time
        val year = SimpleDateFormat("yyyy").format(time)
        val month = SimpleDateFormat("MM").format(time)
        val day = SimpleDateFormat("dd").format(time)
        val refactMonth = if (month.startsWith('0'))
            month.substring(1)
        else month
        //Edit text:
        this.frf1_report_edit_text.setText("№${this.presenter.getReportCount() + 1}/$day.$refactMonth.$year")
        this.frf1_date_edit_text.setText("$refactMonth/$day/$year")
        this.frf1_fuel_volume_edit_text.clearInputText()
        this.frf1_fuel_amount_edit_text.clearInputText()
        this.frf1_start_time_odometr_edit_text.clearInputText()
        this.frf1_start_time_work_edit_text.setText(OregoTimeUtils.getCurrentTime())

        //Spinner:
        this.frf1_name_edit_text.setSelection(0)
        this.frf1_position_edit_text.setSelection(0)
        this.frf1_fuel_type_edit_text.setSelection(0)

        //Photo:
        val defaultCameraDrawable = this.defaultCameraDrawable
        this.frf1_fuel_photo_image_view.setImageDrawable(defaultCameraDrawable)
        this.frf1_odometr_photo_image_view.setImageDrawable(defaultCameraDrawable)
        this.frf1_organization_edit_text.setText(uncompletedReport.organization)
        this.frf1_customer_edit_text.setText(uncompletedReport.customer)
        this.frf1_info_car_edit_text.setText(uncompletedReport.carInfo)
        this.frf1_driver_edit_text.setText(uncompletedReport.driver)
        this.frf1_column_number_edit_text.setText(uncompletedReport.columnNumber)
        this.frf1_site_number_edit_text.setText(uncompletedReport.siteNumber)
        this.frf1_area_name_edit_text.setText(uncompletedReport.objectNameAndAddress)

        this.presenter.reportManager.progress =
            TRepository.WorkerSubRepository.ReportManager.Progress.FIRST_FORM_STARTED
        Log.i("MY_TAG_ILYA", "clearInputText теперь FIRST_FORM_STARTED")
    }

    /**
     * Hide.
     */

    private fun hideErrors() {
        //Value errors:
        this.frf1_report_error_text_view.hide()
        this.frf1_date_error_text_view.hide()
        this.frf1_name_error_text_view.hide()
        this.frf1_position_error_text_view.hide()
        this.frf1_organization_error_text_view.hide()
        this.frf1_customer_error_text_view.hide()
        this.frf1_info_car_error_text_view.hide()
        this.frf1_driver_error_text_view.hide()
        this.frf1_column_number_error_text_view.hide()
        this.frf1_site_number_error_text_view.hide()
        this.frf1_area_name_error_text_view.hide()
        this.frf1_fuel_volume_error_text_view.hide()
        this.frf1_fuel_type_error_text_view.hide()
        this.frf1_fuel_amount_error_text_view.hide()
        this.frf1_start_time_odometr_error_text_view.hide()
        this.frf1_start_time_work_error_text_view.hide()

        //Photo errors:
        this.frf1_fuel_photo_error_text_view.hide()
        this.frf1_odometr_photo_error_text_view.hide()
    }


    /**
     * Spinner.
     */

    private fun fillSpinners() {
        this@TReportForm1Fragment.apply {
            val manual = this.presenter.manualManager.currentManual
            if (manual != null) {
                val currentWorker = this.presenter.accountManager.currentWorker

                //Name:
                val names = mutableListOf<String>()
                if (currentWorker != null) {
                    names.add(currentWorker.name)
                }
                names.addAll(manual.names)
                this.frf1_name_edit_text.setStrings(names.distinct())

                //Position:
                val posts = mutableListOf<String>()
                if (currentWorker != null) {
                    posts.add(currentWorker.post)
                }
                posts.addAll(manual.posts)
                this.frf1_position_edit_text.setStrings(posts)

                initEditTextAdapter(frf1_organization_edit_text, manual.executors.toList())
                initEditTextAdapter(frf1_customer_edit_text, manual.customers.toList())
                initEditTextAdapter(frf1_info_car_edit_text, manual.cars.toList())
                initEditTextAdapter(frf1_driver_edit_text, manual.drivers.toList())
                initEditTextAdapter(frf1_column_number_edit_text, manual.columns.toList())
                initEditTextAdapter(frf1_site_number_edit_text, manual.sites.toList())
                initEditTextAdapter(frf1_area_name_edit_text, manual.objects.toList())

                this.frf1_fuel_type_edit_text.setStrings(
                    manual.fuelTypes.toMutableList()
                )
            }
        }
    }

    private fun initEditTextAdapter(
        autoCompleteTextView: AutoCompleteTextView,
        list: List<String>
    ) {
        val adapter = ArrayAdapter<String>(context!!, android.R.layout.simple_list_item_1, list)
        autoCompleteTextView.setAdapter(adapter)
        autoCompleteTextView.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                autoCompleteTextView.setText("")
            }
        }
        autoCompleteTextView.setOnClickListener {
            if (autoCompleteTextView.text.isEmpty()) {
                autoCompleteTextView.setText("")
            }
        }
    }


    private fun Spinner.setStrings(list: List<String>, callback: (String) -> Unit = {}) {
        this.configure(list, this@TReportForm1Fragment.context!!, android.R.layout.simple_spinner_item, callback)
    }

    /**
     * Presenter.
     */

    inner class Presenter : TAbstractFragment.Presenter(), OregoCameraComponent, OregoToastyComponent, CoroutineScope {

        override val activity
            get() = this@TReportForm1Fragment.activity as TActivity


        override val coroutineContext = Dispatchers.Main

        /**
         * Managers.
         */

        val reportManager: TRepository.WorkerSubRepository.ReportManager
            get() = this.repository.workerSubRepository.reportManager

        val manualManager: TRepository.WorkerSubRepository.ManualManager
            get() = this.repository.workerSubRepository.manualManager

        val accountManager: TRepository.WorkerSubRepository.AccountManager
            get() = this.repository.workerSubRepository.accountManager

        override val gearManager: TGearManager
            get() = this.application.gearManager

        /**
         * Photo.
         */

        private var photoType: PhotoType? = null

        fun takePhoto(photoType: PhotoType) {
            this.photoType = photoType
            this.takePhoto()
        }

        /**
         * Finishes the a form 1.
         */

        fun finish() {
            this@TReportForm1Fragment.refreshReport()
            if (this@TReportForm1Fragment.isValidReport()) {
                this.launch {
                    Log.i("MY_TAG_ILYA", "finish теперь FIRST_FORM_NOT_STARTED")
                    this@Presenter.reportManager.progress =
                        TRepository.WorkerSubRepository.ReportManager.Progress.FIRST_FORM_NOT_STARTED
                    this@Presenter.reportManager.groundUncompletedTitleReport()
                    this@Presenter.reportManager.uncompletedReport = TReport()
                    this@Presenter.reportManager.groundUncompletedReport()
                    this@Presenter.makeSuccessToasty(this@Presenter.context.getString(R.string.form_1_has_saved))
                    this@Presenter.openFragment(TWorkerReportsFragment::class.java)
                }
            }
        }

        override fun onStop() {
            super.onStop()
            if (this.reportManager.progress != TRepository.WorkerSubRepository.ReportManager.Progress.FIRST_FORM_NOT_STARTED) {
                Log.i("MY_TAG_ILYA", "onStop теперь FIRST_FORM_STARTED")
                this.save(TRepository.WorkerSubRepository.ReportManager.Progress.FIRST_FORM_STARTED)
            }
        }

        /**
         * Saves the a form 1:
         */

        fun save(progress: TRepository.WorkerSubRepository.ReportManager.Progress) {
            this@TReportForm1Fragment.refreshReport()
            this.launch {
                this@Presenter.reportManager.progress = progress
                this@Presenter.reportManager.groundUncompletedReport()
            }
        }

        /**
         * Returns a bitmap after taking photo.
         */

        override fun onTakePhoto(bitmap: Bitmap?) {
            if (bitmap == null){
                Toasty.error(context, "Ошибка камеры").show()
                return
            }
            val currentReport = this.reportManager.uncompletedReport
            val fragment = this@TReportForm1Fragment
            val imageView =
                when (this.photoType) {
                    PhotoType.FUEL -> {
                        currentReport.fuelImageBytesBefore = bitmap.toStringBytes()
                        hideListenerErrorCamera(frf1_fuel_photo_error_text_view)
                        fragment.frf1_fuel_photo_image_view
                    }
                    PhotoType.ODOMETR -> {
                        if (currentReport.isOdometr.toBoolean()) {
                            currentReport.odometrImageBytesBefore = bitmap.toStringBytes()
                            currentReport.counterImageBytesBefore = ""
                        } else {
                            currentReport.counterImageBytesBefore = bitmap.toStringBytes()
                            currentReport.odometrImageBytesBefore = ""
                        }
                        hideListenerErrorCamera(frf1_odometr_photo_error_text_view)
                        fragment.frf1_odometr_photo_image_view
                    }
                    null -> null
                }
            imageView?.setImageBitmap(bitmap)
        }

        /**
         * Callback.
         */

        override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
        ) {
            this.onCameraRequestPermissionsResult(requestCode, permissions, grantResults)
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            this.onCameraActivityResult(requestCode, resultCode, data)
        }

        override fun onBackPressed(): Boolean {
            this@Presenter.reportManager.progress =
                TRepository.WorkerSubRepository.ReportManager.Progress.FIRST_FORM_NOT_STARTED
            this@Presenter.reportManager.uncompletedReport = TReport()
            this@Presenter.reportManager.groundUncompletedReport()
            this.openFragment(TWorkerReportsFragment::class.java)
            return true
        }

        fun getReportCount() = this.reportManager.completedReportTitles.size
    }
}