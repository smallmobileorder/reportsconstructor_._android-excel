package com.orego.truckapp.ui.fragment.implementation.admin

import com.orego.truckapp.R
import com.orego.truckapp.api.repo.gearManager.OregoGearManager
import com.orego.truckapp.api.util.common.account.OregoAccountUtils
import com.orego.truckapp.api.util.ui.component.OregoToastyComponent
import com.orego.truckapp.api.util.ui.view.editText.clearInputText
import com.orego.truckapp.api.util.ui.view.editText.getInputText
import com.orego.truckapp.api.util.ui.view.gone
import com.orego.truckapp.api.util.ui.view.hide
import com.orego.truckapp.api.util.ui.view.show
import com.orego.truckapp.repo.TRepository
import com.orego.truckapp.repo.client.TAdminClient
import com.orego.truckapp.repo.client.TWorkerClient
import com.orego.truckapp.repo.gearManager.TGearManager
import com.orego.truckapp.ui.fragment.TAbstractFragment
import com.orego.truckapp.ui.fragment.implementation.common.TCommonStartFragment
import kotlinx.android.synthetic.main.fragment_admin_sign_in.*

class TAdminSignInFragment : TAbstractFragment(R.layout.fragment_admin_sign_in) {

    override val presenter = Presenter()

    /**
     * View created.
     */

    override fun onFirstViewCreated() {
        //Set on click listeners:
        this.configureListeners()
        //Prepare ui components:
        this.resetComponents()
        //Make auto sign in:
        this.tryAutoSignIn()
    }

    /**
     * Makes auto sign in.
     */

    private fun tryAutoSignIn() {
        this.presenter.tryAutoSignIn()
    }

    /**
     * Configure listeners.
     */

    private fun configureListeners() {
        this.fasi_back_button.setOnClickListener {
            this.presenter.onBackPressed()
        }
        this.fasi_sign_in_button.setOnClickListener {
            this.presenter.signIn()
        }
    }

    /**
     * Component management.
     */

    private fun resetComponents() {
        this.hideErrors()
        this.clearInputTexts()
        this.setInputMode()
    }

    private fun hideErrors() {
        this.faci_phone_error_text_view.hide()
        this.faci_email_error_text_view.hide()
    }

    private fun clearInputTexts() {
        this.faci_phone_edit_text.clearInputText()
        this.faci_email_edit_text.clearInputText()
    }

    /**
     * Modes.
     */

    private fun setInputMode() {
        this.fasi_back_button.show()
        this.fasi_sign_in_button.show()
        this.fasi_progress_bar.hide()
    }

    private fun setLoadingMode() {
        this.fasi_sign_in_button.gone()
        this.fasi_back_button.hide()
        this.fasi_progress_bar.show()
    }

    /**
     * Collects phoneNumber & email from ui components.
     */

    private fun buildAuthParams(): AuthParams? {
        //Phone number:
        val phoneNumberEditText = this.faci_phone_edit_text
        val phoneNumber = phoneNumberEditText.getInputText()
        println("Phone: $phoneNumber")
        if (!OregoAccountUtils.isCorrectPhoneNumber(phoneNumber)) {
            this.faci_phone_error_text_view.show()
            return null
        }
        val formattedPhoneNumber = OregoAccountUtils.formatPhoneNumber(phoneNumber)
        //Email:
        val emailEditText = this.faci_email_edit_text
        val email = emailEditText.getInputText()
        println("Email: $email")
        if (!OregoAccountUtils.isCorrectEmail(email)) {
            this.faci_email_error_text_view.show()
            return null
        }
        return AuthParams(formattedPhoneNumber, email)
    }

    /**
     * Presenter.
     */

    inner class Presenter : TAbstractFragment.Presenter(), OregoToastyComponent {

        private val adminSubRepository: TRepository.AdminSubRepository
            get() = this.repository.adminSubRepository

        /**
         * Tries to sign in by cached last worker data.
         */

        fun tryAutoSignIn() {
            //Lock input components:
            this@TAdminSignInFragment.setLoadingMode()
            //Check auto sign in:
            val accountManager = this.adminSubRepository.accountManager
            if (accountManager.tryAutoSignIn()) {
                this.beginSession()
            } else {
                this@TAdminSignInFragment.setInputMode()
            }
        }

        fun signIn() {
            //Reset:
            this@TAdminSignInFragment.hideErrors()
            val params = this@TAdminSignInFragment.buildAuthParams() ?: return
            this@TAdminSignInFragment.setLoadingMode()
            this.gearManager.launchGear(TGearManager.AdminSignInGear::class.java, params)
        }

        /**
         * On sign in finished.
         */

        override fun onGearFinished(gearClazz: Class<out OregoGearManager.Gear>, result: Any?) {
            if (gearClazz == TGearManager.AdminSignInGear::class.java
                && result is TAdminClient.AuthController.ResponseType
            ) {
                //Handle response:
                when (result) {
                    TAdminClient.AuthController.ResponseType.INVALID_AUTH_PARAMS -> {
                        this.stopWithError("Номер телефона занят")
                    }
                    TAdminClient.AuthController.ResponseType.BAD_RESPONSE -> {
                        this.stopWithError("Сервис недоступен")
                    }
                    TAdminClient.AuthController.ResponseType.NO_CONNECTION -> {
                        this.stopWithError(this@TAdminSignInFragment.getString(R.string.connection_is_failed))
                    }
                    TAdminClient.AuthController.ResponseType.SUCCESS -> {
                        this.beginSession()
                    }
                }
            }
        }

        private fun stopWithError(message: String) {
            this.makeErrorToasty(message)
            this@TAdminSignInFragment.setInputMode()
        }

        private fun beginSession() {

            //Clear fragments for current user:
            this.recycleFragment(TAdminGroupsFragment())
            this.recycleFragment(TAdminWorkersFragment())

            //Clear fields on fragment:
            this@TAdminSignInFragment.resetComponents()

            //Fetch workers:
            this.gearManager.launchGear(TGearManager.AdminFetchGroupsGear::class.java)

            //Open menu fragment:
            this.openFragment(TAdminGroupsFragment::class.java)
        }

        override fun onBackPressed(): Boolean {
            this@TAdminSignInFragment.setInputMode()
            this.gearManager.cancelGear(TGearManager.AdminSignInGear::class.java)
            this.openFragment(TCommonStartFragment::class.java)
            return true
        }
    }

    /**
     * Auth params.
     */

    data class AuthParams(
        val phoneNumber: String,
        val email: String
    )
}