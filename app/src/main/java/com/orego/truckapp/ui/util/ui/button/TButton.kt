package com.orego.truckapp.ui.util.ui.button

import android.content.res.Resources
import android.widget.Button
import com.orego.truckapp.R

fun Button.enable(isEnabled: Boolean, resources: Resources) {
    this.isEnabled = isEnabled
    this.background =
        if (isEnabled) {
            resources.getDrawable(R.drawable.bg_blue_active)
        } else {
            resources.getDrawable(R.drawable.bg_grey)
        }
}