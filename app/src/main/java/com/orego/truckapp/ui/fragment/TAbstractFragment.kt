package com.orego.truckapp.ui.fragment

import android.content.Context
import android.content.res.Resources
import com.orego.truckapp.api.repo.gearManager.OregoGearManager
import com.orego.truckapp.api.ui.OregoFragment
import com.orego.truckapp.core.TApplication
import com.orego.truckapp.repo.TRepository
import com.orego.truckapp.repo.gearManager.TGearManager

abstract class TAbstractFragment(layoutId: Int) : OregoFragment(layoutId) {

    override val presenter = Presenter()
    /**
     * Gear params.
     */

    private var isReportMaterialsReady = false

    private var isManualMaterialsReady = false

    var isUndeliveringSending = false


    protected val areGearsReady: Boolean
        get() = this.isReportMaterialsReady && this.isManualMaterialsReady

    override fun onGearFinished(clazz: Class<out OregoGearManager.Gear>, result: Any?) {
        super.onGearFinished(clazz, result)
        when (clazz) {
            TGearManager.WorkerPushReportsGear::class.java -> this.isUndeliveringSending = false
            TGearManager.WorkerFetchReportsGear::class.java -> this.isReportMaterialsReady = true
            TGearManager.WorkerFetchManualGear::class.java -> this.isManualMaterialsReady = true
        }
        this.onManualAndReportGearsFinished()
    }

    open fun onManualAndReportGearsFinished() {

    }

    /**
     * Presenter.
     */

    open inner class Presenter : OregoFragment.Presenter() {

        override val application: TApplication
            get() = super.application as TApplication

        /**
         * Repository.
         */

        val repository: TRepository
            get() = this.application.repository

        /**
         * Context.
         */

        val context: Context
            get() = this.application.applicationContext

        /**
         * Resources.
         */

        val resources: Resources
            get() = this.context.resources
    }
}