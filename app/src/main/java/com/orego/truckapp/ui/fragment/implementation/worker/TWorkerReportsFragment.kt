package com.orego.truckapp.ui.fragment.implementation.worker

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.res.Resources
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.orego.truckapp.R
import com.orego.truckapp.api.repo.gearManager.OregoGearManager
import com.orego.truckapp.api.util.ui.view.gone
import com.orego.truckapp.api.util.ui.view.hide
import com.orego.truckapp.api.util.ui.view.recyclerView.configure
import com.orego.truckapp.api.util.ui.view.show
import com.orego.truckapp.core.TActivity
import com.orego.truckapp.repo.TRepository
import com.orego.truckapp.repo.data.entity.report.TReportTitleEntity
import com.orego.truckapp.repo.gearManager.TGearManager
import com.orego.truckapp.ui.fragment.TAbstractFragment
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_worker_reports.*
import kotlinx.android.synthetic.main.fragment_worker_reports.fwm_sign_out_button
import kotlinx.android.synthetic.main.item_report_info.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class TWorkerReportsFragment : TAbstractFragment(R.layout.fragment_worker_reports) {

    override val presenter = Presenter()

    override fun onFirstViewCreated() {
        this.configureListeners()
        this.configureRecyclerView()
    }

    private fun configureRecyclerView() {
        val context = this.context
        val adapter = this.presenter.adapter
        this.fwr_recycler_view.configure(context, adapter)
        val adapterReports = adapter.reportTitles
        adapterReports.clear()
        adapterReports += this.presenter.reportManager.completedReportTitles
    }


    private fun configureListeners() {
        this.fwr_search_edit_text.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
                val request = s.toString()
                println("SEARCH REQUEST $request")
                this@TWorkerReportsFragment.presenter.search(request)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
        this.fwm_sign_out_button.setOnClickListener {
            signOutDialog()
        }
        this.create_report.setOnClickListener {
            this@TWorkerReportsFragment.presenter.createReport()
        }
    }

    private fun signOutDialog() {
        val builder = AlertDialog.Builder(context)
        builder
            .setMessage("Выйти из профиля?")
            .setCancelable(true)
            .setNegativeButton("Нет") { dialog, _ -> dialog.cancel() }
            .setPositiveButton("Да") { dialog, _ ->
                dialog.cancel()
                presenter.signOut()
            }
        val alert = builder.create()
        alert.show()
    }

    /**
     * Start.
     */

    override fun onStart() {
        super.onStart()
        if (this.areGearsReady) {
            this.start()
        }
    }

    override fun onManualAndReportGearsFinished() {
        if (this.activity == null) {
            return
        }
        this.start()
    }

    private fun start() {
        val reports = this.presenter.reportManager.completedReportTitles
        if (reports.isEmpty()) {
            this.fwr_info_text_view?.show()
            this.fwr_recycler_view?.gone()
        } else {
            this.fwr_info_text_view?.gone()
            this.fwr_recycler_view?.show()
        }

        if (this.isUndeliveringSending) {
            fwr_loading_constraint_layout.show()
            fwr_progress_bar.show()
            sending_raport_text.show()
        } else {
            fwr_loading_constraint_layout.gone()
            fwr_progress_bar.gone()
            sending_raport_text.gone()
        }
        this.configureRecyclerView()
    }

    /**
     * Presenter.
     */

    inner class Presenter : TAbstractFragment.Presenter(), CoroutineScope {

        /**
         * Coroutine context.
         */

        override val coroutineContext = Dispatchers.Main

        /**
         * Report manager.
         */

        val reportManager: TRepository.WorkerSubRepository.ReportManager
            get() = this.repository.workerSubRepository.reportManager

        /**
         * Adapter.
         */

        val adapter by lazy {
            Adapter()
        }

        fun createReport() {
            val nextFragmentClazz = TReportForm1Fragment::class.java
            this.openFragment(nextFragmentClazz)
        }

        fun signOut() {
            this.repository.workerSubRepository.signOut()
            this.openFragment(TWorkerSignInFragment::class.java)
        }

        fun search(request: String) {
            val inputText = request.toLowerCase()
            val adapterReports = this.adapter.reportTitles
            adapterReports.clear()
            this.reportManager.completedReportTitles.forEach { reportTitleEntity ->
                val uiText = this.adapter.buildUiText(reportTitleEntity)
                if (uiText.toLowerCase().contains(inputText)) {
                    adapterReports += reportTitleEntity
                }
            }
            this.adapter.notifyDataSetChanged()
        }

        override fun onGearFinished(gearClazz: Class<out OregoGearManager.Gear>, result: Any?) {
            if (gearClazz == TGearManager.WorkerPushReportsGear::class.java) {
                val response = result as Boolean
                if (response) {
                    val adapterReports = adapter.reportTitles
                    adapterReports.clear()
                    adapterReports += this.reportManager.completedReportTitles
                    if (this@TWorkerReportsFragment::class.java == this.application.fragmentController.displayedFragmentClazz) {
                        this.adapter.notifyDataSetChanged()
                    }
                } else {
                    Toasty.info(context, "Отправка прервана").show()
                }
                fwr_loading_constraint_layout.hide()
                fwr_progress_bar.hide()
                sending_raport_text.hide()
            }
        }

        /**
         * Adapter.
         */

        inner class Adapter : RecyclerView.Adapter<Adapter.ViewHolder>() {

            private val reportViewHolders: MutableMap<Int, RecyclerView.ViewHolder> = mutableMapOf()

            val reportTitles = mutableListOf<TReportTitleEntity>()

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.item_report_info, parent, false)
                return ViewHolder(view)
            }

            override fun onBindViewHolder(holder: ViewHolder, position: Int) {
                this.reportViewHolders[position] = holder
                holder.bindTo(position)
            }

            override fun getItemCount() = this.reportTitles.size

            fun buildUiText(report: TReportTitleEntity): String {
                val number = report.reportNumber
                val personName = report.personName
                val driverName = report.driver
                return "Рапорт $number заполнил $personName водитель $driverName"
            }

            /**
             * View holder.
             */

            inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

                /**
                 * Context.
                 */

                private val resources: Resources
                    get() = this@Presenter.context.resources

                /**
                 * Components.
                 */

                private val headerTextView = itemView.iri_header_text_view

                private val deliveredImageView = itemView.iri_cloud_image_view
                private val helpForm = itemView.help_form2

                private val background = itemView.background_card
                private fun sendUndeliveredReport() {
                    val builder = AlertDialog.Builder(this@TWorkerReportsFragment.context)
                    builder
                        .setTitle("Рапорт еще не был отправлен администратору?")
                        .setMessage("Отправить рапорт?")
                        .setCancelable(true)
                        .setNegativeButton("Нет") { dialog, _ -> dialog.cancel() }
                        .setPositiveButton("Отправить") { dialog, _ ->
                            dialog.cancel()
                            if (checkInternetConnection()) {
                                fwr_loading_constraint_layout.show()
                                sending_raport_text.show()
                                fwr_progress_bar.show()
                                isUndeliveringSending = true
                                val gearManager = application.gearManager
                                gearManager.launchGear(TGearManager.WorkerPushReportsGear::class.java)
                            } else {
                                Toasty.info(context, "Требуется подключение к интернету").show()
                            }
                        }
                    val alert = builder.create()
                    alert.show()
                }

                @SuppressLint("SetTextI18n")
                fun bindTo(position: Int) {
                    val reportTitleEntity = this@Adapter.reportTitles[position]
                    this.headerTextView.text = this@Adapter.buildUiText(reportTitleEntity)
                    if (reportTitleEntity.isComplete) {
                        if (reportTitleEntity.hasDelivered) {
                            background.setOnClickListener {
                                this.openCompleteReport()
                            }
                            helpForm.gone()
                        } else {
                            background.setOnClickListener {
                                sendUndeliveredReport()
                            }
                            helpForm.show()
                            helpForm.text = "Нажмите чтобы отправить рапорт администратору"
                        }
                        deliveredImageView.show()

                        headerTextView.setTextColor(resources.getColor(R.color.black))
                        background.setBackgroundResource(R.drawable.bg_complete_raport)
                        //Set image view:
                        val drawableId =
                            if (reportTitleEntity.hasDelivered) {
                                R.drawable.ic_delivered
                            } else {
                                R.drawable.ic_not_delivered
                            }
                        deliveredImageView.show()
                        val drawable = this.resources.getDrawable(drawableId)
                        this.deliveredImageView.setImageDrawable(drawable)
                    } else {
                        deliveredImageView.hide()
                        background.setOnClickListener {
                            this.openCurrentReport()
                        }
                        headerTextView.setTextColor(resources.getColor(R.color.black_transparent))
                        helpForm.show()
                        helpForm.text = "Нажмите чтобы заполнить вторую часть рапорта"
                        deliveredImageView.gone()
                        background.setBackgroundResource(R.drawable.bg_uncompleted_raport)
                    }
                }

                private fun openCurrentReport() = GlobalScope.launch {
                    val titleEntity = this@Adapter.reportTitles[this@ViewHolder.adapterPosition]
                    val reportId = titleEntity.id
                    val fullReportEntity = this@Presenter.reportManager.digCompletedReport(reportId)

                    this@Presenter.application.workerReportInfoController.entityReport = fullReportEntity
                    this@Presenter.application.workerReportInfoController.titleCurrentId = reportId
                    this@Presenter.openFragment(TReportForm2Fragment::class.java)

                }

                private fun openCompleteReport() = GlobalScope.launch {
                    val titleEntity = this@Adapter.reportTitles[this@ViewHolder.adapterPosition]
                    val reportId = titleEntity.id
                    val fullReportEntity = this@Presenter.reportManager.digCompletedReport(reportId)
                    this@Presenter.application.workerReportInfoController.entityReport = fullReportEntity
                    this@Presenter.openFragment(TWorkerReportInfoFragment::class.java)
                }
            }
        }

        private fun checkInternetConnection(): Boolean {
            val tactivity = activity as TActivity
            return tactivity.isOnline()
        }

    }
}