package com.orego.truckapp.ui.util.photoType

enum class PhotoType {
    FUEL,
    ODOMETR
}