package com.orego.truckapp.ui.fragment.implementation.admin

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.orego.truckapp.R
import com.orego.truckapp.api.repo.gearManager.OregoGearManager
import com.orego.truckapp.api.util.ui.view.gone
import com.orego.truckapp.api.util.ui.view.hide
import com.orego.truckapp.api.util.ui.view.recyclerView.configure
import com.orego.truckapp.api.util.ui.view.show
import com.orego.truckapp.api.util.ui.view.textView.setLimitText
import com.orego.truckapp.repo.TRepository
import com.orego.truckapp.repo.client.TAdminClient
import com.orego.truckapp.repo.client.model.admin.group.worker.TWorker
import com.orego.truckapp.repo.gearManager.TGearManager
import com.orego.truckapp.ui.fragment.TAbstractFragment
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_admin_workers.*
import kotlinx.android.synthetic.main.item_worker_info.view.*

class TAdminWorkersFragment : TAbstractFragment(R.layout.fragment_admin_workers) {

    override val presenter = Presenter()

    /**
     * View created.
     */

    override fun onFirstViewCreated() {
        this.configureListeners()
        this.configureRecyclerView()
    }

    private fun configureListeners() {
        this.faw_back_button.setOnClickListener {
            this.onBackPressed()
        }
        this.faw_create_worker_button.setOnClickListener {
            this.presenter.createWorker()
        }
        this.faw_manual_button.setOnClickListener {
            this.presenter.openManual()
        }
    }

    private fun configureRecyclerView() {
        val context = this.context
        val adapter = this.presenter.adapter
        this.faw_recycler_view.configure(context, adapter)
    }

    private fun start() {
        val groups = this.presenter.workerManager.workers
        if (groups.isEmpty()) {
            this.faw_info_text_view?.show()
            this.faw_recycler_view?.gone()
        } else {
            this.faw_info_text_view?.gone()
            this.faw_recycler_view?.show()
        }
        val controller = this.presenter.application.adminGroupInfoController
        val currentGroup = controller.currentGroup ?: return

        this.name_group?.setLimitText(currentGroup.groupName, 12)
        this.setInputMode()
    }

    private fun setInputMode() {
        this.faw_create_worker_button?.show()
        this.faw_manual_button?.show()
        this.faw_loading_constraint_layout?.gone()
    }

    private fun setLoadingMode() {

        this.faw_create_worker_button?.hide()
        this.faw_info_text_view?.hide()
        this.faw_manual_button?.hide()
        this.faw_recycler_view?.hide()

        this.faw_loading_constraint_layout?.show()

    }

    /**
     * Presenter.
     */

    inner class Presenter : TAbstractFragment.Presenter() {

        /**
         * Report manager.
         */

        val workerManager: TRepository.AdminSubRepository.WorkerManager
            get() = this.repository.adminSubRepository.workerManager

        /**
         * Adapter.
         */

        val adapter by lazy {
            Adapter()
        }

        private var areWorkersFetched = false

        /**
         * Start.
         */

        override fun onStart() {
            super.onStart()
            if (this.areWorkersFetched) {
                this@TAdminWorkersFragment.start()
            }
        }

        override fun onGearFinished(gearClazz: Class<out OregoGearManager.Gear>, result: Any?) {
            when (gearClazz) {
                TGearManager.AdminFetchWorkersGear::class.java -> this.onWorkersFetched(result)
                TGearManager.AdminPostWorkerGear::class.java -> this.onWorkerCreated(result)
            }
        }

        private fun onWorkersFetched(result: Any?) {
            println("WORKERS FETCHED: " + result.toString())
            if (result == TAdminClient.WorkerController.ResponseType.SUCCESS) {
                val adapterWorkers = this.adapter.workers
                adapterWorkers.clear()
                adapterWorkers += this.workerManager.workers
                this.areWorkersFetched = true
                this@TAdminWorkersFragment.start()
                this.adapter.notifyDataSetChanged()
            }
        }

        private fun onWorkerCreated(result: Any?) {
            println("WORKER CREATED: " + result.toString())
            if (result !is Pair<*, *>) {
                return
            }
            val type = result.first
            if (type != TAdminClient.WorkerController.ResponseType.SUCCESS) {
                return
            }
            val worker = result.second as? TWorker ?: return
            val firstIndex = 0
            this.adapter.workers.add(firstIndex, worker)
            this@TAdminWorkersFragment.start()
            this.adapter.notifyItemInserted(firstIndex)
        }

        override fun onBackPressed(): Boolean {
            this.gearManager.cancelGear(TGearManager.AdminFetchWorkersGear::class.java)
            this.application.adminWorkerInfoController.currentWorker = null
            this.areWorkersFetched = false
            this@TAdminWorkersFragment.setLoadingMode()
            this.openFragment(TAdminGroupsFragment::class.java)
            return true
        }

        fun createWorker() {
            this.openFragment(TAdminAddWorkerFragment::class.java)
        }

        fun openManual() {
            //Get manual:
            val controller = this.application.adminGroupInfoController
            val currentGroup = controller.currentGroup ?: return
            val manualUrl = currentGroup.manualUrl
            //Open browser:
            val intent = Intent(Intent.ACTION_VIEW)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.data = Uri.parse(manualUrl)
            this@TAdminWorkersFragment.startActivity(intent)
        }

        /**
         * Adapter.
         */

        inner class Adapter : RecyclerView.Adapter<Adapter.ViewHolder>() {

            private val workerViewHolders: MutableMap<Int, RecyclerView.ViewHolder> = mutableMapOf()

            val workers = mutableListOf<TWorker>()

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.item_worker_info, parent, false)
                return ViewHolder(view)
            }

            override fun onBindViewHolder(holder: ViewHolder, position: Int) {
                this.workerViewHolders[position] = holder
                holder.bindTo(position)
            }

            override fun getItemCount() = this.workers.size

            /**
             * View holder.
             */

            inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

                /**
                 * Components.
                 */

                private val headerTextView = itemView.iwi_worker_name_text_view
                private val login = itemView.iwi_worker_login
                private val password = itemView.iwi_worker_password
                private val numberPhone = itemView.iwi_worker_number_phone
                private val post = itemView.iwi_worker_post

                init {
                    itemView.setOnClickListener {
                        this.openCurrentWorker()
                    }
                }

                @SuppressLint("SetTextI18n")
                fun bindTo(position: Int) {
                    val worker = this@Adapter.workers[position]
                    //Set text:
                    this.headerTextView.text = "Имя: " + worker.name
                    this.login.text = "Логин: " + worker.login
                    this.password.text = "Пароль: " + worker.password
                    if (worker.phone.isNotEmpty())
                        this.numberPhone.text = "Телефон: " + worker.phone
                    else{
                        this.numberPhone.gone()
                    }
                    if (worker.phone.isNotEmpty())
                        this.post.text = "Должность: " + worker.post
                    else{
                        this.post.gone()
                    }
                }

                private fun openCurrentWorker() {
                    val worker = this@Adapter.workers[this@ViewHolder.adapterPosition]
                    //While is empty.
                }
            }
        }
    }
}