package com.orego.truckapp.ui.fragment.implementation.admin

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.orego.truckapp.R
import com.orego.truckapp.api.repo.gearManager.OregoGearManager
import com.orego.truckapp.api.util.ui.view.editText.getInputText
import com.orego.truckapp.api.util.ui.view.gone
import com.orego.truckapp.api.util.ui.view.hide
import com.orego.truckapp.api.util.ui.view.recyclerView.configure
import com.orego.truckapp.api.util.ui.view.show
import com.orego.truckapp.core.TApplication
import com.orego.truckapp.repo.TRepository
import com.orego.truckapp.repo.client.TAdminClient
import com.orego.truckapp.repo.client.model.admin.group.TWorkerGroup
import com.orego.truckapp.repo.gearManager.TGearManager
import com.orego.truckapp.ui.fragment.TAbstractFragment
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.dialog_admin_create_group.*
import kotlinx.android.synthetic.main.dialog_admin_create_group.view.*
import kotlinx.android.synthetic.main.fragment_admin_groups.*
import kotlinx.android.synthetic.main.fragment_admin_groups.fam_exit_button
import kotlinx.android.synthetic.main.item_group_info.view.*

class TAdminGroupsFragment : TAbstractFragment(R.layout.fragment_admin_groups) {

    override val presenter = Presenter()

    /**
     * View created.
     */

    override fun onFirstViewCreated() {
        this.configureListeners()
        this.configureRecyclerView()
        this.fag_create_group_button.gone()
        this.fag_loading_constraint_layout?.show()
    }

    /**
     * Sets click listeners on buttons.
     */

    private fun configureListeners() {
        this.fam_exit_button.setOnClickListener {
            signOutDialog()
        }
        this.fag_create_group_button.setOnClickListener {
            this.presenter.createGroup()
        }
    }

    private fun signOutDialog() {
        val builder = AlertDialog.Builder(context)
        builder
            .setMessage("Выйти из профиля?")
            .setCancelable(true)
            .setNegativeButton("Нет") { dialog, _ -> dialog.cancel() }
            .setPositiveButton("Да") { dialog, _ ->
                dialog.cancel()
                presenter.signOut()
            }
        val alert = builder.create()
        alert.show()
    }

    /**
     * Sets adapter on recycler view.
     */

    private fun configureRecyclerView() {
        val context = this.context
        val adapter = this.presenter.adapter
        this.fag_recycler_view.configure(context, adapter)
    }

    /**
     * Activates input mode for user.
     */

    private fun start() {
        val groups = this.presenter.groupManager.groups
        if (groups.isEmpty()) {
            this.fag_info_text_view?.show()
            this.fag_recycler_view?.gone()
        } else {
            this.fag_info_text_view?.gone()
            this.fag_recycler_view?.show()
        }
        //Hide loading mode:
        this.fag_loading_constraint_layout?.gone()
        this.fag_create_group_button?.show()
    }

    /**
     * Presenter.
     */

    inner class Presenter : TAbstractFragment.Presenter() {

        /**
         * Group manager.
         */

        val groupManager: TRepository.AdminSubRepository.GroupManager
            get() = this.repository.adminSubRepository.groupManager

        /**
         * Adapter.
         */

        val adapter by lazy {
            Adapter()
        }

        /**
         * Indicates that groups have fetched.
         */

        private var areGroupsFetched = false

        /**
         * Start.
         */

        override fun onStart() {
            if (this.areGroupsFetched) {
                this@TAdminGroupsFragment.start()
            }
        }

        override fun onGearFinished(gearClazz: Class<out OregoGearManager.Gear>, result: Any?) {
            when (gearClazz) {
                TGearManager.AdminFetchGroupsGear::class.java -> this.onGroupsFetched(result)
                TGearManager.AdminPostGroupGear::class.java -> this.onGroupCreated(result)
            }
        }

        private fun onGroupsFetched(result: Any?) {
            println("GROUPS FETCHED: " + result.toString())
            if (result == TAdminClient.GroupController.ResponseType.SUCCESS) {
                val newGroups = this.groupManager.groups
                this.adapter.groups.clear()
                this.adapter.groups.addAll(newGroups)
                this.adapter.notifyDataSetChanged()
            }
            this.areGroupsFetched = true
            this@TAdminGroupsFragment.start()
        }

        private fun onGroupCreated(result: Any?) {
            if (result !is Pair<*, *>) {
                return
            }
            val type = result.first
            if (type != TAdminClient.GroupController.ResponseType.SUCCESS) {
                return
            }
            val workerGroup = result.second as? TWorkerGroup ?: return
            println("WORKER GROUP: <><><> " + workerGroup)
            val firstIndex = 0
            this.adapter.groups.add(firstIndex, workerGroup)
            this@TAdminGroupsFragment.fag_info_text_view?.gone()
            this@TAdminGroupsFragment.fag_recycler_view?.show()
            this.adapter.notifyItemInserted(firstIndex)
        }
        

        fun createGroup() {
            val fragmentManager = this@TAdminGroupsFragment.fragmentManager
            CreateGroupDialogFragment.show(fragmentManager, this.application)
        }

        fun signOut() {
            this.repository.adminSubRepository.signOut()
            this.openFragment(TAdminSignInFragment::class.java)
        }

        /**
         * Adapter.
         */

        inner class Adapter : RecyclerView.Adapter<Adapter.ViewHolder>() {

            var groups = mutableListOf<TWorkerGroup>()

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.item_group_info, parent, false)
                return ViewHolder(view)
            }

            override fun onBindViewHolder(holder: ViewHolder, position: Int) {
                holder.bindTo(position)
            }

            override fun getItemCount() = this.groups.size

            /**
             * View holder.
             */

            inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

                /**
                 * Components.
                 */

                private val headerTextView = itemView.igi_group_name_text_view

                init {
                    itemView.setOnClickListener {
                        this.openCurrentGroup()
                    }
                }

                /**
                 * Fills data to item view with data
                 */

                fun bindTo(position: Int) {
                    val groups = this@Adapter.groups
                    val group = groups[position]
                    //Set text:
                    this.headerTextView.text = group.groupName
                }

                /**
                 * Opens current group fragment.
                 */

                private fun openCurrentGroup() {
                    val groups = this@Adapter.groups
                    val group = groups[this.adapterPosition]
                    //Fetch workers:
                    val gearManager = this@Presenter.gearManager
                    gearManager.launchGear(TGearManager.AdminFetchWorkersGear::class.java, group.workersUrl)
                    //Put group info to ui controller:
                    val controller = this@Presenter.application.adminGroupInfoController
                    controller.currentGroup = group
                    //Open group info fragment:
                    this@Presenter.openFragment(TAdminWorkersFragment::class.java)
                }
            }
        }
    }

    /**
     * Dialog.
     */

    class CreateGroupDialogFragment : DialogFragment(), OregoGearManager.Gear.Listener {

        companion object {

            private const val CREATE_GROUP_DIALOG_TAG = "CREATE_GROUP_DIALOG_TAG"

            fun show(fragmentManager: FragmentManager?, application: TApplication) {
                if (fragmentManager != null) {
                    val dialog = CreateGroupDialogFragment()
                    dialog.configure(application)
                    dialog.show(fragmentManager, CREATE_GROUP_DIALOG_TAG)
                }
            }
        }

        /**
         * Context.
         */

        private lateinit var gearManager: TGearManager

        private lateinit var baseContext : Context

        /**
         * Presenter.
         */

        private val presenter = Presenter()

        /**
         * Buttons.
         */

        private val backButton by lazy {
            this.view!!.dacg_back_button
        }

        private val createGroupButton by lazy {
            this.view!!.dacg_create_group_button
        }

        /**
         * Input components.
         */

        private val groupNameHeaderTextView by lazy {
            this.view!!.dacg_name_header_text_view
        }

        private val groupNameEditText by lazy {
            this.view!!.dacg_name_edit_text
        }

        private val groupNameErrorTextView by lazy {
            this.view!!.dacg_group_name_error_text_view
        }

        /**
         * Loading components.
         */

        private val loadingConstraintLayout by lazy {
            this.view!!.dacg_loading_constraint_layout
        }

        private val progressBar by lazy {
            this.view!!.dacg_progress_bar
        }

        private val creatingTextView by lazy {
            this.view!!.dacg_creating_text_view
        }

        /**
         * Configure.
         */

        private fun configure(application: TApplication) {
            this.gearManager = application.gearManager
            this.baseContext = application.baseContext
            this.gearManager.registerListener(
                TGearManager.AdminPostGroupGear::class.java, this
            )
        }

        /**
         * Create view.
         */

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
            return inflater.inflate(R.layout.dialog_admin_create_group, container, false)!!
        }

        /**
         * View created.
         */

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            this.setTitle()
            this.configureListeners()
            this.setInputMode()
            this.isCancelable = false
        }

        private fun setTitle() {
            this.dialog?.setTitle(this.getString(R.string.create_a_worker_group)) //HERE STOPPED!!!
        }

        private fun configureListeners() {
            this.createGroupButton.setOnClickListener {
                this.presenter.createGroup()
            }
            this.backButton.setOnClickListener {
                this.presenter.back()
            }
        }

        /**
         * Component management.
         */

        private fun setInputMode() {
            //Input components:
            this.backButton.show()
            this.createGroupButton.show()
            this.groupNameHeaderTextView.show()
            this.groupNameEditText.show()
            this.groupNameErrorTextView.hide()

            //Loading components:
            this.loadingConstraintLayout.hide()
            this.progressBar.hide()
            this.creatingTextView.hide()
        }

        private fun setLoadingMode() {
            //Input components:
            this.backButton.hide()
            this.createGroupButton.hide()
            this.groupNameHeaderTextView.hide()
            this.groupNameEditText.hide()
            this.groupNameErrorTextView.hide()

            //Loading components:
            this.loadingConstraintLayout.show()
            this.progressBar.show()
            this.creatingTextView.show()
        }

        private fun buildGroup(): TWorkerGroup? {
            this.hideErrors()
            val groupName = this.groupNameEditText.getInputText()
            if (groupName.isEmpty()) {
                this.dacg_group_name_error_text_view.show()
                return null
            }
            return TWorkerGroup(groupName)
        }

        private fun hideErrors() {
            this.dacg_group_name_error_text_view.hide()
        }

        override fun dismiss() {
            super.dismiss()
            this.gearManager.unregisterListener(
                TGearManager.AdminPostGroupGear::class.java, this
            )
        }

        /**
         * On group created.
         */

        override fun onGearFinished(clazz: Class<out OregoGearManager.Gear>, result: Any?) {
            if (clazz != TGearManager.AdminPostGroupGear::class.java) {
                return
            }
            if (result !is Pair<*, *>) {
                return
            }
            val type = result.first as? TAdminClient.GroupController.ResponseType ?: return
            val context = this.baseContext
            this@CreateGroupDialogFragment.setInputMode()
            this.dismiss()
            when (type) {
                TAdminClient.GroupController.ResponseType.IS_EXIST -> {
                    Toasty.error(context, "Группа с таким именем уже существует").show()
                }
                TAdminClient.GroupController.ResponseType.BAD_RESPONSE -> {
                    Toasty.error(context, "Сервис недоступен").show()
                }
                TAdminClient.GroupController.ResponseType.NO_CONNECTION -> {
                    Toasty.error(context, "Отсутствует соедиенение с интернетом").show()
                }
                TAdminClient.GroupController.ResponseType.SUCCESS -> {
                    Toasty.success(context, "Группа успешно создана").show()
                }
            }
        }

        /**
         * Presenter.
         */

        private inner class Presenter {

            fun createGroup() {
                val group = this@CreateGroupDialogFragment.buildGroup() ?: return
                this@CreateGroupDialogFragment.setLoadingMode()
                this@CreateGroupDialogFragment.gearManager.launchGear(
                    TGearManager.AdminPostGroupGear::class.java, group
                )
            }

            fun back() {
                this@CreateGroupDialogFragment.dismiss()
            }
        }
    }
}