# Client flow
The application allows you to automate the process of control of contractors and create an automatic report on the work done using the familiar interface for administration
## Administrator flow 
<img src="screenshots/screen1.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen2.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen3.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen4.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen6.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen5.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen7.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen8.jpg" alt="drawing" width="200"/>

## Worker flow

<img src="screenshots/screen11.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen9.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen12.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen13.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen14.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen15.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen16.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen17.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen18.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen19.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen20.jpg" alt="drawing" width="200"/>
<img src="screenshots/screen10.jpg" alt="drawing" width="200"/>

# Google Drive flow
<h3> State with the installation of cloud space for further work</h3>
<img src="screenshots/backScreen21.48.png" alt="drawing" width="800"/>
<h5> Inner contents Administrator folder: </h5>
<img src="screenshots/backScreen22.12.png" alt="drawing" width="800"/>
<h3> State after the registration of the Administrator on client version </h3>
<img src="screenshots/backScreen26.51.png" alt="drawing" width="800"/>
<h5> Inner contents administartors excel file:</h5>
<img src="screenshots/backScreen27.04.png" alt="drawing" width="800"/>
<h3> State after the registration of the Group Workers on client version</h3>
<img src="screenshots/backScreen29.46.png" alt="drawing" width="800"/>
<h5>Inner contents group workers excel file:</h5>
<img src="screenshots/backScreen29.24.png" alt="drawing" width="800"/>
<h3> State after registration two user group wokers</h3>
<img src="screenshots/backScreen37.18.png" alt="drawing" width="800"/>
<h5>Inner contents participant worker group excel file: </h5>
<img src="screenshots/backScreen38.32.png" alt="drawing" width="800"/>
<h5> Hanbook allows you to produce auto complete text while filling in the report. Inner contents handbook:</h5>
<img src="screenshots/backScreen40.29.png" alt="drawing" width="800"/>
<h5> Inner contents report:</h5>
<img src="screenshots/backScreen50.19.png" alt="drawing" width="800"/>